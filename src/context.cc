// context.cc - load and save context

// Copyright (C) 2021 Luis Sanz <luis.sanz@gmail.com>

// This file is part of Wodox 2021.

// Wodox 2021 is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Wodox 2021 is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with Wodox 2021.  If not, see <https://www.gnu.org/licenses/>.


#include "context.h"

#include <fstream>

#include "mdea/mdeaxx.h"
#include "xdgbds.h"

struct Context_loader {
        Context context;
        std::array<Mob_type, 128> indices;

        Context operator()(const std::filesystem::path& path)
        {
                if (auto doc = mdea::document{path}) {
                        parse(doc.root().get_object());
                        return std::move(context);
                }
                throw std::runtime_error{path.string() + ": bad JSON"};
        }

        void parse(const mdea::object& obj)
        {
                parse_size(obj["size"].get_array());
                if (auto x = obj["indices"])
                        parse_indices(x.get_array());
                else
                        generate_indices();
                parse_data(obj["data"].get_array());
                if (auto x = obj["music"])
                        context.music = x.get_string();
        }

        void parse_size(const mdea::array& arr)
        {
                if (arr.size() != 3)
                        throw std::runtime_error{"bad world size"};
                context.box = cells16b(0L, arr[0].get_long(),
                                0L, arr[1].get_long(),
                                0L, arr[2].get_long());
        }

        void parse_indices(const mdea::array& arr)
        {
                auto it = indices.begin();
                for (auto x : arr)
                        *it++ = mob_type_name(x.get_string());
        }

        void generate_indices()
        {
                for (size_t i = 0; i < indices.size(); ++i)
                        indices[i] = (Mob_type)i;
        }

        void parse_data(const mdea::array& arr)
        {
                auto it = arr.begin();
                cells16v p;
                for (p.z = context.box.f; p.z != context.box.b; ++p.z)
                for (p.y = context.box.d; p.y != context.box.u; ++p.y)
                for (p.x = context.box.l; p.x != context.box.r; ++p.x)
                        context.table.emplace(p, Cell{indices[(*it++).get_long()]});
        }
};

Context load_context(const std::filesystem::path& path)
{
        Context_loader load;
        return load(path);
}

std::ostream& operator<<(std::ostream& stream, const cells16& num)
{ return stream << (int)num; }

std::ostream& operator<<(std::ostream& stream, const cells16v& vec)
{ return stream << "[" << vec.x << "," << vec.y << "," << vec.z << "]"; }

struct Context_saver {
        const Context& context;
        std::ofstream stream;
        std::array<int, 128> indices;

        Context_saver(const Context& context, const std::filesystem::path& path)
                : context{context}, stream{path.string()}
        { if (!stream) throw std::runtime_error{"Can't open " + path.string()}; }

        void generate_indices()
        {
                stream << "[";
                int max_index = 0;
                std::fill(indices.begin(), indices.end(), -1);
                for (const auto& x : context.table)
                        if (indices[x.second.index] == -1) {
                                indices[x.second.index] = max_index;
                                if (max_index != 0) stream << ",";
                                stream << "\"" << mob_type_name(x.second.type) << "\"";
                                ++max_index;
                        }
                stream << "]";
        }

        void generate_table()
        {
                cells16v p;
                stream << "[\n                ";
                const char* sep = "";
                for (p.z = context.box.f; p.z != context.box.b; ++p.z) {
                        for (p.y = context.box.u; p.y != context.box.d; ++p.y) {
                                for (p.x = context.box.l; p.x != context.box.r; ++p.x) {
                                        stream << sep << indices[context.table.at(p).index];
                                        sep = ",";
                                }
                                sep = ",\n                ";
                        }
                        sep = ",\n\n                ";
                }
                stream << "\n        ]";
        }

        void operator()()
        {
                stream << "{\n";
                stream << "        \"music\":" << context.music << ",\n";
                stream << "        \"size\":" << context.box.size() << ",\n";
                stream << "        \"indices\":";
                generate_indices();
                stream << ",\n";
                stream << "        \"data\":";
                generate_table();
                stream << "\n";
                stream << "}\n";
        }
};

void save_context(const Context& context, const std::filesystem::path& path)
{
        Context_saver{context, path}();
}
