// edit.h - primary logic for editor

// Copyright (C) 2021 Luis Sanz <luis.sanz@gmail.com>

// This file is part of Wodox 2021.

// Wodox 2021 is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Wodox 2021 is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with Wodox 2021.  If not, see <https://www.gnu.org/licenses/>.


#ifndef SRC_EDIT_H_
#define SRC_EDIT_H_

#include "media.h"
#include "render.h"
#include "config.h"

struct Input {
        bool left : 1;
        bool right : 1;
        bool down : 1;
        bool up : 1;
        bool front : 1;
        bool back : 1;
        bool paint : 1;
        bool erase : 1;
        bool pick : 1;
        bool quit : 1;
        Mob_type color = Mob_type::fixed;
};

class Audio {
 private:
        Subsystem_lock<SDL_INIT_AUDIO> slock;
        std::vector<Chunk> chunks;

 public:
        Audio(const Audio_config& config);
        ~Audio();
        void update(Context& context);
};

class Camera {
 private:
        glm::vec3 position_;
        float rotation_x_;
        float rotation_y_;
        glm::vec3 direction_;
        Mvp mvp_;

 public:
        Camera(const cells16v& size);
        void update(Input input);
        const Mvp& mvp() const { return mvp_; }
        const glm::vec3& position() const { return position_; }
        const glm::vec3& direction() const { return direction_; }
        void position(const glm::vec3& new_position) { position_ = new_position; }
        void direction(const glm::vec3& new_direction) { direction_ = new_direction; }
};

class Handle_events {
 private:
        Subsystem_lock<SDL_INIT_GAMECONTROLLER> slock;
        const Edit_input_config& config;
        std::vector<Gamepad> gamepads;
        std::string command;

        std::string update_status(const Input& input);
        void handle_event(const SDL_Event& event, Input& input);
        void handle_text(const SDL_Event& event, Input& input);
        void handle_raw(const SDL_Event& event, Input& input);
        bool parse_command(Input& input);

 public:
        Handle_events(const Edit_input_config& config);
        void operator()(Input& input);
        std::string status() const;
};

class Update : public Main_loop<Update> {
 private:
        Input input{0};
        std::filesystem::path path;
        Context context;
        Renderer renderer;
        Audio audio;
        Camera camera;
        Handle_events handle_events;
        int cooldown = 0;
        bool must_save = false;

        void update_circuits();
        void update_status();
        void update_cursor();

 public:
        Update(const Config& config, const std::filesystem::path& path);
        bool update();
};

#endif // SRC_EDIT_H_
