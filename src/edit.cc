// edit.cc - primary logic for editor

// Copyright (C) 2021 Luis Sanz <luis.sanz@gmail.com>

// This file is part of Wodox 2021.

// Wodox 2021 is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Wodox 2021 is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with Wodox 2021.  If not, see <https://www.gnu.org/licenses/>.


#include "edit.h"

#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>

#include "config.h"
#include "xdgbds.h"
#include "nearest.h"
#include "option.h"
#include "input.h"

// Argument list parsing

const char usage_string[] =
"Usage: " BASEDIR "-edit --help\n"
"       " BASEDIR "-edit --version\n"
"       " BASEDIR "-edit --config\n"
"       " BASEDIR "-edit --input\n"
"       " BASEDIR "-edit <level file>...\n"
"\n";

const char option_string[] =
"Options:\n"
"  -h, --help           display this help and exit\n"
"  -v, --version        display version string and exit\n"
"  -c, --config         create configuration file\n"
"  -i, --input          query inputs and create configuration file\n"
"\n";

void query_inputs()
{
        Config config = load_config();
        Query_input query_input;
        config.edit.left = query_input("left");
        config.edit.right = query_input("right");
        config.edit.front = query_input("front");
        config.edit.back = query_input("back");
        config.edit.up = query_input("up");
        config.edit.down = query_input("down");
        config.edit.paint = query_input("paint");
        config.edit.erase = query_input("erase");
        config.edit.pick = query_input("pick");
        save_config(config);
}

std::vector<std::string_view> parse_options(char** first, char** last)
{
        option::Option_parser option_parser = {
                { 'h', "help", false },
                { 'v', "version", false },
                { 'c', "config", false },
                { 'i', "input", false },
        };

        std::vector<std::string_view> levels;

        for (auto opt : option_parser(first, last)) {
                switch (opt.first.short_str) {
                 case 0:
                        levels.push_back(opt.second);
                        break;
                 case 'h':
                        std::cout << usage_string;
                        std::cout << option_string;
                        exit(EXIT_SUCCESS);
                 case 'v':
                        std::cout << VERSION << "\n";
                        exit(EXIT_SUCCESS);
                 case 'c':
                        save_config(Config{});
                        exit(EXIT_SUCCESS);
                 case 'i':
                        query_inputs();
                        exit(EXIT_SUCCESS);
                }
        }

        // At least one level file name must be passed as argument.
        if (levels.size() != 1) {
                std::cerr << usage_string;
                exit(EXIT_FAILURE);
        }

        return levels;
}

// Application entrypoint

int main(int argc, char *argv[])
{
        try {
                auto levels = parse_options(argv + 1, argv + argc);
                Update{load_config(), levels[0]}();
                return 0;
        } catch (std::exception& ex) {
                std::cerr << ex.what() << "\n";
                return -1;
        }
}

// Manage all audio for the editor.

Audio::Audio(const Audio_config& conf)
{
        if (Mix_OpenAudio(conf.frequency, conf.format, conf.channels, conf.chunksize) != 0)
                throw std::runtime_error{Mix_GetError()};

        Mix_Volume(-1, conf.sfx_volume);
        Mix_VolumeMusic(conf.music_volume);

        chunks.push_back(load_sound(xdg::data::find(BASEDIR "/sounds/player.flac")));
        chunks.push_back(load_sound(xdg::data::find(BASEDIR "/sounds/press.flac")));
        chunks.push_back(load_sound(xdg::data::find(BASEDIR "/sounds/release.flac")));
        chunks.push_back(load_sound(xdg::data::find(BASEDIR "/sounds/land.flac")));
        chunks.push_back(load_sound(xdg::data::find(BASEDIR "/sounds/camera.flac")));
        chunks.push_back(load_sound(xdg::data::find(BASEDIR "/sounds/write.flac")));
        chunks.push_back(load_sound(xdg::data::find(BASEDIR "/sounds/flip.flac")));
}

Audio::~Audio()
{
        Mix_CloseAudio();
}

void Audio::update(Context& context)
{
        for (size_t i = 0; i < chunks.size(); ++i)
                if (context.sounds[i])
                        Mix_PlayChannel(-1, chunks[i].get(), 0);
        context.sounds = 0;
}

// Manage all the camera logic for the editor
//
// When reset, the camera is placed at the back of the level, looking directly
// to its center.
//
// Updating the camera includes all camera movement:
//
// - Use mouse position to change camera rotation, then reposition the mouse in
//   the center of the window
// - Use player input to move the camera according to its current rotation.
// - Recalculate mvp matrix.

Camera::Camera(const cells16v& size)
{
        auto [width, height] = get_viewport();
        SDL_WarpMouseInWindow(NULL, width / 2, height / 2);
        position_ = glm::vec3((int)size.x / 2, (int)size.y / 2, (int)size.z + 20);
        rotation_x_ = 0;
        rotation_y_ = 0;
}

void Camera::update(Input input)
{
        auto [width, height] = get_viewport();
        float ratio = (float)width / (float)height;
        static const float speed = 0.1f;

        int x, y;
        SDL_GetMouseState(&x, &y);
        SDL_WarpMouseInWindow(NULL, width / 2, height / 2);
        x -= width / 2;
        y -= height / 2;
        if (abs(x) < width / 4) rotation_y_ -= x * 0.005;
        if (abs(y) < height / 4) rotation_x_ -= y * 0.005;
        rotation_x_ = std::clamp<float>(rotation_x_, -glm::half_pi<float>(), glm::half_pi<float>());
        glm::vec4 offset(0, 0, -1, 1);
        offset = glm::rotate(rotation_x_, glm::vec3(1, 0, 0)) * offset;
        offset = glm::rotate(rotation_y_, glm::vec3(0, 1, 0)) * offset;
        direction_ = offset;

        if (input.left) position_ += glm::normalize(glm::vec3(offset.z, 0, -offset.x)) * speed;
        if (input.right) position_ -= glm::normalize(glm::vec3(offset.z, 0, -offset.x)) * speed;
        if (input.up) position_.y += speed;
        if (input.down) position_.y -= speed;
        if (input.front) position_ += glm::normalize(glm::vec3(offset.x, 0, offset.z)) * speed;
        if (input.back) position_ -= glm::normalize(glm::vec3(offset.x, 0, offset.z)) * speed;

        mvp_.position = position_;
        mvp_.projection = glm::perspective(glm::radians(45.0f), ratio, 0.1f, 100.0f);
        mvp_.view = glm::lookAt(position_, position_ + direction_, glm::vec3(0, 1, 0));
        mvp_.model = glm::mat4(1.0f);
}

// Manages events for the game
//
// At creation time, the class opens every single joystick and game controller
// it can find. This is probably not a good idea, but I'm still experimenting
// with the concept a little.
//
// The update function will consume every queued event from SDL and pass them
// to the event catchers defined in the configuration table, then update the
// input struct with the resulting values.

Handle_events::Handle_events(const Edit_input_config& config) : config{config}
{
        gamepads = open_all_gamepads();
        SDL_StopTextInput(); // Apparently the system starts on text input?
}

void Handle_events::operator()(Input& input)
{
        SDL_Event event;
        while (SDL_PollEvent(&event))
                handle_event(event, input);
}

void Handle_events::handle_event(const SDL_Event& event, Input& input)
{
        static const Quit_catcher quit{};

        if (SDL_IsTextInputActive())
                handle_text(event, input);
        else
                handle_raw(event, input);
        input.quit = catch_event(event, quit, input.quit);
}

void Handle_events::handle_text(const SDL_Event& event, Input& input)
{
        static const Scancode_catcher backspace_key{SDL_SCANCODE_BACKSPACE};
        static const Scancode_catcher escape_key{SDL_SCANCODE_ESCAPE};

        if (event.type == SDL_TEXTINPUT) {
                command += event.text.text;
                if (!parse_command(input)) return;
                command.clear();
                SDL_StopTextInput();
        } else if (event.type == SDL_TEXTEDITING) {
                command.replace(event.edit.start, event.edit.length, event.edit.text);
                if (!parse_command(input)) return;
                command.clear();
                SDL_StopTextInput();
        } else if (catch_event(event, backspace_key, false)) {
                if (!command.empty())
                        command.pop_back();
        } else if (catch_event(event, escape_key, false)) {
                command.clear();
                SDL_StopTextInput();
        }
}

void Handle_events::handle_raw(const SDL_Event& event, Input& input)
{
        static const Scancode_catcher escape_key{SDL_SCANCODE_ESCAPE};
        static const Scancode_catcher period_key{SDL_SCANCODE_PERIOD};

        input.left = catch_event(event, config.left, input.left);
        input.right = catch_event(event, config.right, input.right);
        input.up = catch_event(event, config.up, input.up);
        input.down = catch_event(event, config.down, input.down);
        input.front = catch_event(event, config.front, input.front);
        input.back = catch_event(event, config.back, input.back);
        input.paint = catch_event(event, config.paint, input.paint);
        input.erase = catch_event(event, config.erase, input.erase);
        input.pick = catch_event(event, config.pick, input.pick);
        input.quit = catch_event(event, escape_key, input.quit);
        if (catch_event(event, period_key, false))
                SDL_StartTextInput();
}

bool Handle_events::parse_command(Input& input)
{
        if (command == "f") { input.color = Mob_type::fixed; return true; }
        if (command == "p") { input.color = Mob_type::player; return true; }
        if (command == "c") { input.color = Mob_type::crate; return true; }
        if (command == "l") { input.color = Mob_type::lift; return true; }
        if (command == "h") { input.color = Mob_type::hover; return true; }
        if (command == "b") { input.color = Mob_type::button; return true; }
        if (command == "B") { input.color = Mob_type::plug; return true; }
        if (command == "w") { input.color = Mob_type::wire; return true; }
        if (command == "k") { input.color = Mob_type::checkpoint; return true; }

        if (command == "el") { input.color = Mob_type::engine_left; return true; }
        if (command == "er") { input.color = Mob_type::engine_right; return true; }
        if (command == "eu") { input.color = Mob_type::engine_up; return true; }
        if (command == "ed") { input.color = Mob_type::engine_down; return true; }
        if (command == "ef") { input.color = Mob_type::engine_front; return true; }
        if (command == "eb") { input.color = Mob_type::engine_back; return true; }

        if (command == "nl") { input.color = Mob_type::buffer_left; return true; }
        if (command == "nr") { input.color = Mob_type::buffer_right; return true; }
        if (command == "nu") { input.color = Mob_type::buffer_up; return true; }
        if (command == "nd") { input.color = Mob_type::buffer_down; return true; }
        if (command == "nf") { input.color = Mob_type::buffer_front; return true; }
        if (command == "nb") { input.color = Mob_type::buffer_back; return true; }

        if (command == "Nl") { input.color = Mob_type::not_left; return true; }
        if (command == "Nr") { input.color = Mob_type::not_right; return true; }
        if (command == "Nu") { input.color = Mob_type::not_up; return true; }
        if (command == "Nd") { input.color = Mob_type::not_down; return true; }
        if (command == "Nf") { input.color = Mob_type::not_front; return true; }
        if (command == "Nb") { input.color = Mob_type::not_back; return true; }

        if (command == "ol") { input.color = Mob_type::or_left; return true; }
        if (command == "or") { input.color = Mob_type::or_right; return true; }
        if (command == "ou") { input.color = Mob_type::or_up; return true; }
        if (command == "od") { input.color = Mob_type::or_down; return true; }
        if (command == "of") { input.color = Mob_type::or_front; return true; }
        if (command == "ob") { input.color = Mob_type::or_back; return true; }

        if (command == "Ol") { input.color = Mob_type::nor_left; return true; }
        if (command == "Or") { input.color = Mob_type::nor_right; return true; }
        if (command == "Ou") { input.color = Mob_type::nor_up; return true; }
        if (command == "Od") { input.color = Mob_type::nor_down; return true; }
        if (command == "Of") { input.color = Mob_type::nor_front; return true; }
        if (command == "Ob") { input.color = Mob_type::nor_back; return true; }

        if (command == "al") { input.color = Mob_type::and_left; return true; }
        if (command == "ar") { input.color = Mob_type::and_right; return true; }
        if (command == "au") { input.color = Mob_type::and_up; return true; }
        if (command == "ad") { input.color = Mob_type::and_down; return true; }
        if (command == "af") { input.color = Mob_type::and_front; return true; }
        if (command == "ab") { input.color = Mob_type::and_back; return true; }

        if (command == "Al") { input.color = Mob_type::nand_left; return true; }
        if (command == "Ar") { input.color = Mob_type::nand_right; return true; }
        if (command == "Au") { input.color = Mob_type::nand_up; return true; }
        if (command == "Ad") { input.color = Mob_type::nand_down; return true; }
        if (command == "Af") { input.color = Mob_type::nand_front; return true; }
        if (command == "Ab") { input.color = Mob_type::nand_back; return true; }

        if (command == "xl") { input.color = Mob_type::xor_left; return true; }
        if (command == "xr") { input.color = Mob_type::xor_right; return true; }
        if (command == "xu") { input.color = Mob_type::xor_up; return true; }
        if (command == "xd") { input.color = Mob_type::xor_down; return true; }
        if (command == "xf") { input.color = Mob_type::xor_front; return true; }
        if (command == "xb") { input.color = Mob_type::xor_back; return true; }

        if (command == "Xl") { input.color = Mob_type::xnor_left; return true; }
        if (command == "Xr") { input.color = Mob_type::xnor_right; return true; }
        if (command == "Xu") { input.color = Mob_type::xnor_up; return true; }
        if (command == "Xd") { input.color = Mob_type::xnor_down; return true; }
        if (command == "Xf") { input.color = Mob_type::xnor_front; return true; }
        if (command == "Xb") { input.color = Mob_type::xnor_back; return true; }

        if (command == "g") { input.color = Mob_type::goal; return true; }

        return false;
}

std::string Handle_events::status() const
{
        if (SDL_IsTextInputActive())
                return command + "\02";
        return "press \".\" to enter command";
}

// The editor itself starts here.

Context load_context_for_edit(const std::filesystem::path& path)
{
        try {
                return load_context(path);
        } catch (std::exception& ex) {
                Context context;
                context.table.emplace(cells16v(0, 0, 0), Cell{{Mob_type::fixed}});
                return context;
        }
}

Update::Update(const Config& config, const std::filesystem::path& path) :
        Main_loop{config.window},
        path{path},
        context{load_context_for_edit(path)},
        audio{config.audio},
        camera{context.box.max() - context.box.min()},
        handle_events{config.edit}
{
        context.mobs.push_back(Mob{geom::center(context.box), Mob_type::cursor});
        context.mobs.push_back(Mob{geom::center(context.box), Mob_type::cursor_2});
}

bool Update::update()
{
        handle_events(input);
        if (input.quit) {
                if (must_save) {
                        save_context(context, path);
                }
                return true;
        }
        update_cursor();
        camera.update(input);
        update_circuits();
        update_status();
        renderer.update(context);
        renderer.render(camera.mvp(), -10.f);
        audio.update(context);
        return false;
}

// Update status line

void Update::update_circuits()
{
        if (context.tick > 0)
                return;
        for (auto& cell : context.table) {
                switch (cell.second.type) {
                 case Mob_type::wire:
                 case Mob_type::button:
                 case Mob_type::plug:
                 case Mob_type::buffer_left:
                 case Mob_type::buffer_right:
                 case Mob_type::buffer_up:
                 case Mob_type::buffer_down:
                 case Mob_type::buffer_front:
                 case Mob_type::buffer_back:
                 case Mob_type::not_left:
                 case Mob_type::not_right:
                 case Mob_type::not_up:
                 case Mob_type::not_down:
                 case Mob_type::not_front:
                 case Mob_type::not_back:
                 case Mob_type::or_left:
                 case Mob_type::or_right:
                 case Mob_type::or_up:
                 case Mob_type::or_down:
                 case Mob_type::or_front:
                 case Mob_type::or_back:
                 case Mob_type::nor_left:
                 case Mob_type::nor_right:
                 case Mob_type::nor_up:
                 case Mob_type::nor_down:
                 case Mob_type::nor_front:
                 case Mob_type::nor_back:
                 case Mob_type::and_left:
                 case Mob_type::and_right:
                 case Mob_type::and_up:
                 case Mob_type::and_down:
                 case Mob_type::and_front:
                 case Mob_type::and_back:
                 case Mob_type::nand_left:
                 case Mob_type::nand_right:
                 case Mob_type::nand_up:
                 case Mob_type::nand_down:
                 case Mob_type::nand_front:
                 case Mob_type::nand_back:
                 case Mob_type::xor_left:
                 case Mob_type::xor_right:
                 case Mob_type::xor_up:
                 case Mob_type::xor_down:
                 case Mob_type::xor_front:
                 case Mob_type::xor_back:
                 case Mob_type::xnor_left:
                 case Mob_type::xnor_right:
                 case Mob_type::xnor_up:
                 case Mob_type::xnor_down:
                 case Mob_type::xnor_front:
                 case Mob_type::xnor_back:
                        cell.second.powered = power_off;
                        break;
                 default:
                        cell.second.powered = no_power;
                        break;
                }
        }
}

void Update::update_status()
{
        std::ostringstream status;
        Mob_type target = (context.mobs[0].type == Mob_type::none) ?
                Mob_type::none : context.table[context.mobs[0].pos].type;
        status << "[target:" << mob_type_name(target);
        status << ", color:" << mob_type_name(input.color) << "] ";
        status << handle_events.status();
        context.status = status.str();
}

// Update the player mob according to player input and camera rotation.

void Update::update_cursor()
{
        auto best = nearest_cell(context.table, context.box, camera.position(), camera.direction());
        if (!best.found) {
                context.mobs[0].type = Mob_type::none;
                context.mobs[1].type = Mob_type::none;
                return;
        }
        context.mobs[0].type = Mob_type::cursor;
        context.mobs[1].type = Mob_type::cursor_2;
        context.mobs[0].pos = best.position;
        context.mobs[1].pos = best.position + best.normal;
        if (cooldown > 0) {
                --cooldown;
                return;
        }
        if (input.paint) {
                auto it = context.table.find(context.mobs[1].pos);
                if (it == context.table.end())
                        it = context.table.emplace(context.mobs[1].pos, Cell{}).first;
                it->second.type = input.color;
                context.sounds[(size_t)Sound::press] = true;
                context.update_static = true;
                context.update_mutable = true;
                cooldown = 8;
                must_save = true;
        } else if (input.erase) {
                context.table.erase(context.mobs[0].pos);
                context.sounds[(size_t)Sound::release] = true;
                context.update_static = true;
                context.update_mutable = true;
                cooldown = 8;
                must_save = true;
        } else if (input.pick) {
                auto it = context.table.find(context.mobs[0].pos);
                input.color = it->second.type;
                context.sounds[(size_t)Sound::camera] = true;
                cooldown = 8;
        }
}

