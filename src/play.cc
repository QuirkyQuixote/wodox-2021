// play.cc - primary game logic

// Copyright (C) 2021 Luis Sanz <luis.sanz@gmail.com>

// This file is part of Wodox 2021.

// Wodox 2021 is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Wodox 2021 is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with Wodox 2021.  If not, see <https://www.gnu.org/licenses/>.


#include "play.h"

#include "config.h"
#include "xdgbds.h"
#include "option.h"
#include "input.h"

// Argument parsing

const char usage_string[] =
"Usage: " BASEDIR "-play --help\n"
"       " BASEDIR "-play --version\n"
"       " BASEDIR "-play --config\n"
"       " BASEDIR "-play --input\n"
"       " BASEDIR "-play <level file>...\n"
"\n";

const char option_string[] =
"Options:\n"
"  -h, --help           display this help and exit\n"
"  -v, --version        display version string and exit\n"
"  -c, --config         create configuration file\n"
"  -i, --input          query inputs and create configuration file\n"
"\n";

void query_inputs()
{
        Config config = load_config();
        Query_input query_input;
        config.game.left = query_input("left");
        config.game.right = query_input("right");
        config.game.up = query_input("up");
        config.game.down = query_input("down");
        config.game.cw = query_input("cw");
        config.game.ccw = query_input("ccw");
        save_config(config);
}

std::vector<std::string_view> parse_options(char** first, char** last)
{
        option::Option_parser option_parser = {
                { 'h', "help", false },
                { 'v', "version", false },
                { 'c', "config", false },
                { 'i', "input", false },
        };

        std::vector<std::string_view> levels;

        for (auto opt : option_parser(first, last)) {
                switch (opt.first.short_str) {
                 case 0:
                        levels.push_back(opt.second);
                        break;
                 case 'h':
                        std::cout << usage_string;
                        std::cout << option_string;
                        exit(EXIT_SUCCESS);
                 case 'v':
                        std::cout << VERSION << "\n";
                        exit(EXIT_SUCCESS);
                 case 'c':
                        save_config(Config{});
                        exit(EXIT_SUCCESS);
                 case 'i':
                        query_inputs();
                        exit(EXIT_SUCCESS);
                }
        }

        // At least one level file name must be passed as argument.
        if (levels.size() != 1) {
                std::cerr << usage_string;
                exit(EXIT_FAILURE);
        }

        return levels;
}

// Application entrypoint

int main(int argc, char *argv[])
{
        try {
                auto levels = parse_options(argv + 1, argv + argc);
                Update{load_config(), levels[0]}();
                return 0;
        } catch (std::exception& ex) {
                std::cerr << ex.what() << "\n";
                return -1;
        }
}

// All audio for the game

Audio::Audio(const Audio_config& conf)
{
        if (Mix_OpenAudio(conf.frequency, conf.format, conf.channels, conf.chunksize) != 0)
                throw std::runtime_error{Mix_GetError()};

        Mix_Volume(-1, conf.sfx_volume);
        Mix_VolumeMusic(conf.music_volume);

        chunks.push_back(load_sound(xdg::data::find(BASEDIR "/sounds/player.flac")));
        chunks.push_back(load_sound(xdg::data::find(BASEDIR "/sounds/press.flac")));
        chunks.push_back(load_sound(xdg::data::find(BASEDIR "/sounds/release.flac")));
        chunks.push_back(load_sound(xdg::data::find(BASEDIR "/sounds/land.flac")));
        chunks.push_back(load_sound(xdg::data::find(BASEDIR "/sounds/camera.flac")));
        chunks.push_back(load_sound(xdg::data::find(BASEDIR "/sounds/write.flac")));
        chunks.push_back(load_sound(xdg::data::find(BASEDIR "/sounds/flip.flac")));
}

Audio::~Audio()
{
        Mix_CloseAudio();
}

// If the level is reloaded, and there is no music playing, attempt to load and
// play the music whose path is in the context.
//
// For every bit that is on in the sounds mask, play the appropriate sample,
// then set the mask to zero.

void Audio::update_music(Context& context)
{
        if (context.tick > 0 || Mix_PlayingMusic())
                return;
        try {
                if (context.music.empty())
                        return;
                auto path = xdg::data::find(BASEDIR / context.music);
                music = load_music(path);
                Mix_PlayMusic(music.get(), -1);
        } catch (std::exception& ex) {
                std::cerr << ex.what() << "\n";
        }
}

void Audio::update_sounds(Context& context)
{
        if (context.tick > 0)
                for (size_t i = 0; i < chunks.size(); ++i)
                        if (context.sounds[i])
                                Mix_PlayChannel(-1, chunks[i].get(), 0);
        context.sounds = 0;
}

void Audio::update(Context& context)
{
        update_music(context);
        update_sounds(context);
}

// Calculates camera position and model/view/projection matrix from the player
// position in the world and the desired camera angle.

static const glm::vec3 camera_offset_list[] = {
        {-5.f, 20.f, 20.f},
        {20.f, 20.f, 5.f},
        {5.f, 20.f, -20.f},
        {-20.f, 20.f, -5.f},
};

void Camera::update_vectors(Context& context)
{
        if (context.tick == 0) offset = camera_offset_list[angle_];
        else offset = (offset + camera_offset_list[angle_]) / 2.f;
        target = mob_position(context.mobs[context.player]);
        target.y = std::max(target.y, 0.f);
        auto size = context.box.max() - context.box.min();
        if (size.x <= 10_cl) target.x = (int)size.x / 2;
        if (size.z <= 10_cl) target.z = (int)size.z / 2;
        position = target + offset;

}

void Camera::update_mvp()
{
        auto [width, height] = get_viewport();
        float ratio = (float)width / (float)height;

        mvp_.position = position;
        mvp_.projection = glm::perspective(glm::radians(45.0f), ratio, 0.1f, 100.0f);
        mvp_.view = glm::lookAt(position, target, glm::vec3(0, 1, 0));
        mvp_.model = glm::mat4(1.0f);
}

void Camera::update(Context& context)
{
        update_vectors(context);
        update_mvp();
}

// Manages events for the game
//
// At creation time, the class opens every single joystick and game controller
// it can find. This is probably not a good idea, but I'm still experimenting
// with the concept a little.
//
// The update function will consume every queued event from SDL and pass them
// to the event catchers defined in the configuration table, then update the
// input struct with the resulting values.

Handle_events::Handle_events(const Game_input_config& config) : config{config}
{
        gamepads = open_all_gamepads();
}

void Handle_events::operator()(Input& input)
{
        SDL_Event event;
        while (SDL_PollEvent(&event))
                handle_event(event, input);
}

void Handle_events::handle_event(const SDL_Event& event, Input& input)
{
        static const std::vector<Event_catcher> quit = {
                Scancode_catcher{SDL_SCANCODE_ESCAPE},
                Quit_catcher{}
        };

        input.left = catch_event(event, config.left, input.left);
        input.right = catch_event(event, config.right, input.right);
        input.up = catch_event(event, config.up, input.up);
        input.down = catch_event(event, config.down, input.down);
        input.cw = catch_event(event, config.cw, input.cw);
        input.ccw = catch_event(event, config.ccw, input.ccw);
        input.quit = catch_event(event, quit, input.quit);
}

Context load_context_for_game(const std::filesystem::path& path)
{
        Context context = load_context(path);
        context.box.l -= 2_cl;
        context.box.r += 2_cl;
        context.box.d -= 10_cl;
        context.box.u += 10_cl;
        context.box.f -= 2_cl;
        context.box.b += 2_cl;
        return context;
}

Update::Update(const Config& config, const std::filesystem::path& path) :
        Main_loop{config.window},
        context{load_context_for_game(path)},
        audio{config.audio},
        handle_events{config.game},
        update_mobs{context},
        update_circuits{context}
{
        checkpoint = context;
}

bool Update::update()
{
        handle_events(input);
        update_level();
        auto player_dir = update_player();
        update_mobs(context, player_dir);
        update_circuits(context);
        update_camera();
        renderer.update(context);
        renderer.render(camera.mvp(), -10.f);
        audio.update(context);
        ++context.tick;
        return input.quit;
}

// If the player mob is dead, reload the level; this happens when the player
// falls outside the level.

void Update::update_level()
{
        if (context.mobs[context.player].type == Mob_type::none) {
                context = checkpoint;
                context.sounds[(size_t)Sound::write] = true;
                context.status = "Game loaded";
                context.update_static = true;
                context.update_mutable = true;
        }
}

// Update the player mob according to player input and camera rotation.

int Update::update_player()
{
        if (context.player == -1)
                return -1;
        auto& mob = context.mobs[context.player];
        if (mob.frac <= 0) {
                auto& dest = context.table[mob.pos + move_traits<Mob_move::down>::direction];
                if (dest.type == Mob_type::checkpoint) {
                        dest.type = Mob_type::fixed;
                        checkpoint = context;
                        context.sounds[(size_t)Sound::write] = true;
                        context.status = "Game saved";
                        context.update_static = true;
                        context.update_mutable = true;
                } else if (dest.type == Mob_type::goal) {
                        input.quit = true;
                }
        }
        int ret = input.left ? (4 - camera.angle()) % 4 :
                input.up ? (5 - camera.angle()) % 4 :
                input.right ? (6 - camera.angle()) % 4 :
                input.down ? (7 - camera.angle()) % 4 :
                -1;
        if (mob.frac == 0) {
                auto flipped = mob.flipped;
                if (ret == 0 || ret == 2)
                        mob.flipped = false;
                else if (ret == 1 || ret == 3)
                        mob.flipped = true;
                if (mob.flipped != flipped)
                        context.sounds[(size_t)Sound::flip] = true;
        }
        return ret;
}

// Update the camera

void Update::update_camera()
{
        if (input.ccw) {
                camera.angle((camera.angle() + 3) % 4);
                context.sounds[(size_t)Sound::camera] = true;
                input.ccw = false;
        }
        if (input.cw) {
                camera.angle((camera.angle() + 1) % 4);
                context.sounds[(size_t)Sound::camera] = true;
                input.cw = false;
        }
        camera.update(context);
}
