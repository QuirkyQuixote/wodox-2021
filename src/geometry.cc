// geometry.cc - build vertex lists from models

// Copyright (C) 2023 Luis Sanz <luis.sanz@gmail.com>

// This file is part of Wodox 2021.

// Wodox 2021 is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Wodox 2021 is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with Wodox 2021.  If not, see <https://www.gnu.org/licenses/>.

#include "geometry.h"

// Builds geometry for static elements read from the world table

struct Static_geometry_builder {
        std::vector<Vertex> vertices;
        const Model_data& model_data;
        const Cell_table& table;

        bool is_opaque(Mob_type type)
        {
                switch (type) {
                 case Mob_type::none:
                 case Mob_type::player:
                 case Mob_type::lift:
                 case Mob_type::hover:
                 case Mob_type::shadow_left:
                 case Mob_type::shadow_right:
                 case Mob_type::shadow_up:
                 case Mob_type::shadow_down:
                 case Mob_type::shadow_front:
                 case Mob_type::shadow_back:
                 case Mob_type::cursor:
                 case Mob_type::cursor_2:
                        return false;
                 default:
                        return true;
                }
        }

        bool cull_face(Cull cull, cells16v p)
        {
                if (cull == Cull::none) return false;
                auto it = table.find(p +
                        (cull == Cull::left ? cells16v(-1, 0, 0) :
                         cull == Cull::right ? cells16v(1, 0, 0) :
                         cull == Cull::down ? cells16v(0, -1, 0) :
                         cull == Cull::up ? cells16v(0, 1, 0) :
                         cull == Cull::front ? cells16v(0, 0, -1) :
                         cull == Cull::back ? cells16v(0, 0, 1) :
                         cells16v(0, 0, 0)));
                return it != table.end() && it->second.index > 0 &&
                        is_opaque(it->second.type);
        }

        void push_vertex(const Vertex& vert, cells16v p)
        {
                glm::vec3 position((int)p.x, (int)p.y, (int)p.z);
                vertices.push_back(Vertex{vert.position + position, vert.normal,
                                vert.texcoord, vert.tangent, vert.bitangent});;
        }

        void push_face(const Face& face, cells16v p)
        {
                if (cull_face(face.cull, p))
                        return;
                for (auto n = face.first_vertex; n != face.last_vertex; ++n)
                        push_vertex(model_data.vertices[n], p);
        }

        void push_variant(const Variant& variant, cells16v p)
        {
                for (auto n = variant.first_face; n != variant.last_face; ++n)
                        push_face(model_data.faces[n], p);
        }

        Static_geometry_builder(
                        const Model_data& model_data,
                        const Cell_table& table,
                        bool(*filter)(Mob_type))
                : model_data{model_data}, table{table}
        {
                for (auto it = table.begin(); it != table.end(); ++it) {
                        Mob_type type =
                                it->second.gravity == upwards_gravity ? Mob_type::lift :
                                it->second.gravity == no_gravity ? Mob_type::hover :
                                it->second.index > 0 ? it->second.type :
                                Mob_type::none;
                        if (!filter(type)) continue;
                        const auto& model = model_data.models[(int)type];
                        int num_variants = model.last_variant - model.first_variant;
                        if (num_variants == 0) continue;
                        auto p = it->first;
                        int variant =
                                it->second.powered == power_off ? 0 :
                                it->second.powered == power_on ? 1 :
                                ((int)p.x ^ (int)p.y ^ (int)p.z);
                        variant = model.first_variant + variant % num_variants;
                        push_variant(model_data.variants[variant], p);
                }
        }
};

std::vector<Vertex> build_static_geometry(
                const Model_data& model_data,
                const Cell_table& table,
                bool(*filter)(Mob_type))
{
        Static_geometry_builder impl{model_data, table, filter};
        return std::move(impl.vertices);
}

// Builds geometry for moving elements read from the Mob list

struct Moving_geometry_builder {
        const Model_data& model_data;
        std::vector<Vertex> vertices;
        std::vector<Geometry> variants;

        void push_vertex(const Vertex& vertex)
        {
                vertices.push_back(vertex);
        }

        void push_face(const Face& face)
        {
                for (auto i = face.first_vertex; i != face.last_vertex; ++i)
                        push_vertex(model_data.vertices[i]);
        }

        void push_variant(const Variant& variant)
        {
                for (auto i = variant.first_face; i != variant.last_face; ++i)
                        push_face(model_data.faces[i]);
                variants.emplace_back();
                variants.back().update(vertices);
                vertices.clear();
        }

        void push_model(const Model& model)
        {
                for (auto i = model.first_variant; i != model.last_variant; ++i)
                        push_variant(model_data.variants[i]);
        }

        Moving_geometry_builder(const Model_data& model_data)
                : model_data{model_data}
        {
                for (const auto& model : model_data.models)
                        push_model(model);
        }
};

std::vector<Geometry> build_moving_geometry(const Model_data& model_data)
{
        Moving_geometry_builder impl{model_data};
        return std::move(impl.variants);
}

