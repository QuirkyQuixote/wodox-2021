// render.cc - implement scene rendering

// Copyright (C) 2021 Luis Sanz <luis.sanz@gmail.com>

// This file is part of Wodox 2021.

// Wodox 2021 is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Wodox 2021 is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with Wodox 2021.  If not, see <https://www.gnu.org/licenses/>.


#include "render.h"

#include "xdgbds.h"

#include "geometry.h"

// Vertex attributes for a quad that fills the entire screen
// Used by all post-processing render stages.
// Each line is x, y, u, v

// A Quad is a geometry set that contains the vertices for a rectangle covering
// exactly the screen. Used to post-process textures.

Quad::Quad()
{
        static const float vertices[] = {
                -1.0f,  1.0f,  0.0f, 1.0f,
                -1.0f, -1.0f,  0.0f, 0.0f,
                1.0f, -1.0f,  1.0f, 0.0f,
                -1.0f,  1.0f,  0.0f, 1.0f,
                1.0f, -1.0f,  1.0f, 0.0f,
                1.0f,  1.0f,  1.0f, 1.0f
        };
        glBindVertexArray(*vao);
        glBindBuffer(GL_ARRAY_BUFFER, *vbo);
        glBufferData(GL_ARRAY_BUFFER, sizeof(vertices),
                        &vertices, GL_STATIC_DRAW);
        glEnableVertexAttribArray(0);
        glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE,
                        4 * sizeof(float), (void*)0);
        glEnableVertexAttribArray(1);
        glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE,
                        4 * sizeof(float), (void*)(2 * sizeof(float)));
}

void Quad::render()
{
        glBindVertexArray(*vao);
        glDrawArrays(GL_TRIANGLES, 0, 6);
}

// Vertex attributes for a cube that fills the entire screen
// Used to render skyboxes
// Each line is x, y, z

// A Cube is a geometry set that contains the vertices for a cube centered on
// the origin. Used to render skyboxes

Cube::Cube()
{
        static const float vertices[] = {
                -1.0f,  1.0f, -1.0f,
                -1.0f, -1.0f, -1.0f,
                1.0f, -1.0f, -1.0f,
                1.0f, -1.0f, -1.0f,
                1.0f,  1.0f, -1.0f,
                -1.0f,  1.0f, -1.0f,

                -1.0f, -1.0f,  1.0f,
                -1.0f, -1.0f, -1.0f,
                -1.0f,  1.0f, -1.0f,
                -1.0f,  1.0f, -1.0f,
                -1.0f,  1.0f,  1.0f,
                -1.0f, -1.0f,  1.0f,

                1.0f, -1.0f, -1.0f,
                1.0f, -1.0f,  1.0f,
                1.0f,  1.0f,  1.0f,
                1.0f,  1.0f,  1.0f,
                1.0f,  1.0f, -1.0f,
                1.0f, -1.0f, -1.0f,

                -1.0f, -1.0f,  1.0f,
                -1.0f,  1.0f,  1.0f,
                1.0f,  1.0f,  1.0f,
                1.0f,  1.0f,  1.0f,
                1.0f, -1.0f,  1.0f,
                -1.0f, -1.0f,  1.0f,

                -1.0f,  1.0f, -1.0f,
                1.0f,  1.0f, -1.0f,
                1.0f,  1.0f,  1.0f,
                1.0f,  1.0f,  1.0f,
                -1.0f,  1.0f,  1.0f,
                -1.0f,  1.0f, -1.0f,

                -1.0f, -1.0f, -1.0f,
                -1.0f, -1.0f,  1.0f,
                1.0f, -1.0f, -1.0f,
                1.0f, -1.0f, -1.0f,
                -1.0f, -1.0f,  1.0f,
                1.0f, -1.0f,  1.0f
        };
        glBindVertexArray(*vao);
        glBindBuffer(GL_ARRAY_BUFFER, *vbo);
        glBufferData(GL_ARRAY_BUFFER, sizeof(vertices),
                        &vertices, GL_STATIC_DRAW);
        glEnableVertexAttribArray(0);
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE,
                        3 * sizeof(float), (void*)0);
}

void Cube::render()
{
        glBindVertexArray(*vao);
        glDrawArrays(GL_TRIANGLES, 0, 36);
}

// Geometry are collections of vertices that can be rendered.
// The vertex list can be updated with update()

Geometry::Geometry()
{
        glBindVertexArray(*vao);
        glBindBuffer(GL_ARRAY_BUFFER, *vbo);
        glEnableVertexAttribArray(0);
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE,
                        sizeof(Vertex), (void*) 0);
        glEnableVertexAttribArray(1);
        glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE,
                        sizeof(Vertex), (void*) offsetof(Vertex, normal));
        glEnableVertexAttribArray(2);
        glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE,
                        sizeof(Vertex), (void*) offsetof(Vertex, texcoord));
        glEnableVertexAttribArray(3);
        glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE,
                        sizeof(Vertex), (void*) offsetof(Vertex, tangent));
        glEnableVertexAttribArray(4);
        glVertexAttribPointer(4, 3, GL_FLOAT, GL_FALSE,
                        sizeof(Vertex), (void*) offsetof(Vertex, bitangent));
}

void Geometry::update(const std::vector<Vertex>& data)
{
        glBindBuffer(GL_ARRAY_BUFFER, *vbo);
        glBufferData(GL_ARRAY_BUFFER, data.size() * sizeof(Vertex),
                        data.data(), GL_STATIC_DRAW);
        size = data.size();
}

void Geometry::render()
{
        glBindVertexArray(*vao);
        glDrawArrays(GL_TRIANGLES, 0, size);
}

// The geometry renderer renders all objects in the scene to two separate
// textures: one that holds the regular image, and only that only contains
// those fragments whose brightness goes over an specific threshold (see the
// fragment shaders for details).

Geometry_renderer::Geometry_renderer()
{
        auto [width, height] = get_viewport();

        glBindFramebuffer(GL_FRAMEBUFFER, *fbo);

        for (int i = 0; i < 2; ++i) {
                glBindTexture(GL_TEXTURE_2D, *(color_buffer[i]));
                glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F, width, height, 0,
                                GL_RGBA, GL_FLOAT, NULL);
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
                glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 + i,
                                GL_TEXTURE_2D, *(color_buffer[i]), 0);
        }

        glBindRenderbuffer(GL_RENDERBUFFER, *depth_buffer);
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, width, height);
        glBindRenderbuffer(GL_RENDERBUFFER, 0);
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT,
                        GL_RENDERBUFFER, *depth_buffer);

        GLuint attachments[2] = { GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1 };
        glDrawBuffers(2, attachments);

        if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
                throw std::runtime_error{"Framebuffer is not complete!"};

        glBindFramebuffer(GL_FRAMEBUFFER, 0);

        moving_geometry = build_moving_geometry(model_data);
}

void Geometry_renderer::update(Context& context)
{
        if (context.update_static) {
                static_geometry.update(build_static_geometry(
                                        model_data, context.table, is_static));
                context.update_static = false;
        }
        if (context.update_mutable) {
                mutable_geometry.update(build_static_geometry(
                                        model_data, context.table, is_mutable));
                context.update_mutable = false;
        }
        mobs.clear();
        for (const auto& mob : context.mobs) {
                const auto& model = model_data.models[(size_t)mob.type];
                if (model.first_variant == model.last_variant)
                        continue;
                mobs.emplace_back(model.first_variant + mob.flipped,
                                glm::translate(glm::mat4(1.f), mob_position(mob)));
        }
}

std::pair<const Texture&, const Texture&>
Geometry_renderer::render(const Mvp& mvp, float dark_level)
{
        glBindFramebuffer(GL_FRAMEBUFFER, *fbo);
        glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // we're not using the stencil buffer now

        glDisable(GL_CULL_FACE);
        glDisable(GL_DEPTH_TEST);
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_CUBE_MAP, *skybox_texture);
        glUseProgram(*skybox_program);
        skybox_mvp = mvp.projection * glm::mat4(glm::mat3(mvp.view));
        cube.render();

        glEnable(GL_CULL_FACE);
        glEnable(GL_DEPTH_TEST);
        glDepthFunc(GL_LESS);
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, *texture);

        glUseProgram(*geometry_program);
        geometry_projection = mvp.projection;
        geometry_view = mvp.view;
        geometry_model = mvp.model;
        geometry_texture1 = 0;
        geometry_dark_level = dark_level;
        geometry_camera = mvp.position;
        static_geometry.render();
        mutable_geometry.render();
        for (const auto& mob : mobs) {
                geometry_model = mob.second;
                moving_geometry[mob.first].render();
        }

        return std::pair<const Texture&, const Texture&>(color_buffer[0], color_buffer[1]);
}

// The gaussian blur renderer renders a single textured quad and averages the
// value of its pixels in sequencial stages, first horizontally, then
// vertically. The source of the first rendering is a texture passed to
// update(), for the rest of the operations, two textures switch the source and
// destination role between them.

Gaussian_blur::Gaussian_blur()
{
        auto [width, height] = get_viewport();

        for (int i = 0; i < 2; ++i) {
                glBindFramebuffer(GL_FRAMEBUFFER, *(fbo[i]));
                glBindTexture(GL_TEXTURE_2D, *(texture[i]));
                glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F, width, height, 0,
                                GL_RGBA, GL_FLOAT, NULL);
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
                glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
                                GL_TEXTURE_2D, *(texture[i]), 0);
        }
}

const Texture& Gaussian_blur::render(const Texture& input, int passes)
{
        glUseProgram(*program);
        glDisable(GL_DEPTH_TEST);
        for (int i = 0; i < passes; i++)
        {
                glBindFramebuffer(GL_FRAMEBUFFER, *(fbo[0]));
                program_horizontal = true;
                glBindTexture(GL_TEXTURE_2D, (i == 0) ? *input : *(texture[1]));
                quad.render();
                glBindFramebuffer(GL_FRAMEBUFFER, *(fbo[1]));
                program_horizontal = false;
                glBindTexture(GL_TEXTURE_2D, *(texture[0]));
                quad.render();
        }
        glBindFramebuffer(GL_FRAMEBUFFER, 0);
        return texture[1];
}

// For the final stage, two textures are reendered to the screen using additive
// blending. No actual shader tricks here, just GL_BLEND goodness.

void Post_processor::render(const Texture& color, const Texture& bright)
{
        glBindFramebuffer(GL_FRAMEBUFFER, 0);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glDisable(GL_DEPTH_TEST);
        glUseProgram(*program);

        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, *color);
        quad.render();

        glEnable(GL_BLEND);
        glBlendFunc(GL_ONE, GL_ONE);
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, *bright);
        quad.render();
        glDisable(GL_BLEND);
}

// Simple text renderer

Text_renderer::Text_renderer()
{
        glBindVertexArray(*vao);
        glBindBuffer(GL_ARRAY_BUFFER, *vbo);
        glEnableVertexAttribArray(0);
        glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE,
                        sizeof(Vertex), (void*) 0);
        glEnableVertexAttribArray(1);
        glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE,
                        sizeof(Vertex), (void*) offsetof(Vertex, u));
}

void Text_renderer::update(const std::string_view& str)
{
        auto [width, height] = get_viewport();
        std::vector<Vertex> data;
        int i = 0;
        for (auto c : str) {
                GLfloat x0 = i * 8;
                GLfloat x1 = x0 + 8;
                GLfloat y0 = height - 16;
                GLfloat y1 = height;
                GLfloat u0 = (c % 16) / 16.f;
                GLfloat u1 = u0 + 1.0 / 16.f;
                GLfloat v0 = (c / 16) / 16.f;
                GLfloat v1 = v0 + 1.0 / 16.f;
                data.push_back({x0, y0, u0, v0});
                data.push_back({x1, y0, u1, v0});
                data.push_back({x1, y1, u1, v1});
                data.push_back({x0, y0, u0, v0});
                data.push_back({x1, y1, u1, v1});
                data.push_back({x0, y1, u0, v1});
                ++i;
        }
        glBindBuffer(GL_ARRAY_BUFFER, *vbo);
        glBufferData(GL_ARRAY_BUFFER, data.size() * sizeof(Vertex),
                        data.data(), GL_STATIC_DRAW);
        size = data.size();
}

void Text_renderer::render()
{
        auto [width, height] = get_viewport();
        glDisable(GL_CULL_FACE);
        glDisable(GL_DEPTH_TEST);
        glEnable(GL_BLEND);
        glBlendFunc(GL_ONE, GL_ONE);
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, *texture);
        glUseProgram(*program);
        program_mvp = glm::ortho<float>(0, width, height, 0, 0, 1);
        program_image = 0;
        glBindVertexArray(*vao);
        glDrawArrays(GL_TRIANGLES, 0, size);
        glDisable(GL_BLEND);
}

