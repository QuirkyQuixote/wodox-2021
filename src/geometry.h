// geometry.h - build vertex lists from models

// Copyright (C) 2023 Luis Sanz <luis.sanz@gmail.com>

// This file is part of Wodox 2021.

// Wodox 2021 is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Wodox 2021 is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with Wodox 2021.  If not, see <https://www.gnu.org/licenses/>.

#ifndef SRC_GEOMETRY_H_
#define SRC_GEOMETRY_H_

#include "render.h"

#include "xdgbds.h"

// Builds geometry for static elements read from the world table

std::vector<Vertex> build_static_geometry(const Model_data& model_data,
                const Cell_table& table, bool(*filter)(Mob_type));

// Builds geometry for moving elements read from the Mob list

std::vector<Geometry> build_moving_geometry(const Model_data& model_data);

inline bool is_static(Mob_type mob_type)
{
        switch (mob_type) {
         case Mob_type::fixed:
         case Mob_type::lift:
         case Mob_type::hover:
                return true;
         default:
                return false;
        }
}

inline bool is_mutable(Mob_type mob_type)
{ return !is_static(mob_type); }

#endif // SRC_GEOMETRY_H_
