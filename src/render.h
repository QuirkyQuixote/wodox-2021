// render.h - declarations for scene rendering

// Copyright (C) 2021 Luis Sanz <luis.sanz@gmail.com>

// This file is part of Wodox 2021.

// Wodox 2021 is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Wodox 2021 is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with Wodox 2021.  If not, see <https://www.gnu.org/licenses/>.


#ifndef SRC_RENDER_H_
#define SRC_RENDER_H_

#include <vector>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "media.h"
#include "context.h"
#include "model.h"
#include "xdgbds.h"

// Model-view-projection matrices, to be passed around.

struct Mvp {
        glm::vec3 position;
        glm::mat4 projection;
        glm::mat4 view;
        glm::mat4 model;
};

// Holds geometry to render a quad that covers the entire screen
// Can be render by calling render()

class Quad {
 private:
        Vertex_array vao;
        Buffer vbo;

 public:
        Quad();
        void render();
};

// Holds geometry to render a cube that covers the entire screen
// Can be rendered by callin render()

class Cube {
 private:
        Vertex_array vao;
        Buffer vbo;

 public:
        Cube();
        void render();
};

// Holds an OpenGL vertex buffer
// The buffer contents can be modified by calling update().
// The buffer is rendered by calling render().

class Geometry {
 private:
        Vertex_array vao;
        Buffer vbo;
        size_t size;

 public:
        Geometry();
        void update(const std::vector<Vertex>& data);
        void render();
};

// First rendering stage

class Geometry_renderer {
 private:
        Frame_buffer fbo;
        Texture color_buffer[2];
        Render_buffer depth_buffer;
        Cube cube;
        Texture skybox_texture = load_cubemap(
                xdg::data::find(BASEDIR "/textures/skybox_1.png"),
                xdg::data::find(BASEDIR "/textures/skybox_3.png"),
                xdg::data::find(BASEDIR "/textures/skybox_5.png"),
                xdg::data::find(BASEDIR "/textures/skybox_6.png"),
                xdg::data::find(BASEDIR "/textures/skybox_2.png"),
                xdg::data::find(BASEDIR "/textures/skybox_4.png")
                );
        Texture texture = load_image(
                xdg::data::find(BASEDIR "/textures/textures.png")
                );

        Program skybox_program{
                vertex_shader(xdg::data::find(BASEDIR "/shaders/skybox.vert")),
                fragment_shader(xdg::data::find(BASEDIR "/shaders/skybox.frag"))
        };
        Uniform<glm::mat4> skybox_mvp{skybox_program, "MVP"};

        Program geometry_program{
                vertex_shader(xdg::data::find(BASEDIR "/shaders/geometry.vert")),
                fragment_shader(xdg::data::find(BASEDIR "/shaders/geometry.frag"))
        };
        Uniform<glm::mat4> geometry_projection{geometry_program, "projection"};
        Uniform<glm::mat4> geometry_view{geometry_program, "view"};
        Uniform<glm::mat4> geometry_model{geometry_program, "model"};
        Uniform<GLint> geometry_texture1{geometry_program, "texture1"};
        Uniform<GLfloat> geometry_dark_level{geometry_program, "dark_level"};
        Uniform<glm::vec3> geometry_camera{geometry_program, "camera"};

        Model_data model_data = load_model_data(xdg::data::find(BASEDIR "/models/index.json"));
        Geometry static_geometry;
        Geometry mutable_geometry;
        std::vector<Geometry> moving_geometry;
        std::vector<std::pair<size_t, glm::mat4>> mobs;

 public:
        Geometry_renderer();
        void update(Context& context);
        std::pair<const Texture&, const Texture&> render(const Mvp& mvp, float dark_level);
};

// Gaussian blur

class Gaussian_blur {
 private:
        Quad quad;
        Frame_buffer fbo[2];
        Texture texture[2];
        Program program{
                vertex_shader(xdg::data::find(BASEDIR "/shaders/gauss.vert")),
                fragment_shader(xdg::data::find(BASEDIR "/shaders/gauss.frag"))
        };
        Uniform<GLint> program_horizontal{program, "horizontal"};

 public:
        Gaussian_blur();
        const Texture& render(const Texture& input, int passes);
};

// Final rendering stage to merge geometry and blurred lights

class Post_processor {
 private:
        Quad quad;
        Program program{
                vertex_shader(xdg::data::find(BASEDIR "/shaders/final.vert")),
                fragment_shader(xdg::data::find(BASEDIR "/shaders/final.frag"))
        };

 public:
        void render(const Texture& color, const Texture& bright);
};

// Renderer for text

class Text_renderer {
 private:
        struct Vertex { GLfloat x, y, u, v; };
        Vertex_array vao;
        Buffer vbo;
        Texture texture = load_image(xdg::data::find(BASEDIR "/textures/font.png"));
        Program program{
                vertex_shader(xdg::data::find(BASEDIR "/shaders/text.vert")),
                fragment_shader(xdg::data::find(BASEDIR "/shaders/text.frag"))
        };
        Uniform<glm::mat4> program_mvp{program, "mvp"};
        Uniform<GLint> program_image{program, "image"};
        size_t size = 0;

 public:
        Text_renderer();
        void update(const std::string_view& str);
        void render();
};

// General rendering

class Renderer {
 private:
        Geometry_renderer geometry_renderer;
        Gaussian_blur gaussian_blur;
        Post_processor post_processor;
        Text_renderer text_renderer;

 public:
        Renderer() = default;

        void update(Context& context)
        {
                geometry_renderer.update(context);
                text_renderer.update(context.status);
        }

        void render(const Mvp& mvp, float dark_level)
        {
                const auto& [color, bright] = geometry_renderer.render(mvp, dark_level);
                const auto& blurred = gaussian_blur.render(bright, 1);
                post_processor.render(color, blurred);
                text_renderer.render();
        }
};

// Actual position of a Mob in the scene

inline glm::vec3 mob_position(const Mob& mob)
{
        auto p = mob.pos;
        auto q = glm::vec3((int)p.x, (int)p.y, (int)p.z);
        switch (mob.move) {
         case Mob_move::left: q.x += mob.frac / 64.f; break;
         case Mob_move::right: q.x -= mob.frac / 64.f; break;
         case Mob_move::down: q.y += mob.frac / 64.f; break;
         case Mob_move::up: q.y -= mob.frac / 64.f; break;
         case Mob_move::front: q.z += mob.frac / 64.f; break;
         case Mob_move::back: q.z -= mob.frac / 64.f; break;
         default: break;
        }
        return q;
}

#endif // SRC_RENDER_H_
