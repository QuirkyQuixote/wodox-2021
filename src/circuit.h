// circuit.h - implement circuits

// Copyright (C) 2021 Luis Sanz <luis.sanz@gmail.com>

// This file is part of Wodox 2021.

// Wodox 2021 is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Wodox 2021 is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with Wodox 2021.  If not, see <https://www.gnu.org/licenses/>.


#ifndef SRC_CIRCUIT_H_
#define SRC_CIRCUIT_H_

#include "context.h"

class Update_circuits {
 private:
        std::vector<cells16v> triggers;
        std::vector<cells16v> wires;

        // Recursively find wires adjacent to the given position. Use the
        // marked flag in the table cell to avoid passing twice through the
        // same element.
        //
        // Returns true if a powered component is found during the walk
        // pointing to one of the wire cells.

        template<Mob_move Move> bool find_adjacent_wires(Context& context, cells16v p)
        {
                p += move_traits<Move>::direction;
                auto& c = context.table[p];
                if (move_traits<move_traits<Move>::reverse>::is_component(c.type))
                        return c.powered == power_on;
                if (c.type != Mob_type::wire)
                        return false;
                if (c.marked)
                        return false;
                c.marked = true;
                wires.push_back(p);
                bool ret = false;
                if (find_adjacent_wires<Mob_move::left>(context, p)) ret = true;
                if (find_adjacent_wires<Mob_move::right>(context, p)) ret = true;
                if (find_adjacent_wires<Mob_move::up>(context, p)) ret = true;
                if (find_adjacent_wires<Mob_move::down>(context, p)) ret = true;
                if (find_adjacent_wires<Mob_move::front>(context, p)) ret = true;
                if (find_adjacent_wires<Mob_move::back>(context, p)) ret = true;
                return ret;
        }

        // Propagate power
        //
        // The powered variable starts with the powered value of the cell that
        // triggered the propagation, then this function flood-fills every wire
        // cell in search of triggers. If a trigger is found, and it is
        // powered, the powered variable is set to on.
        //
        // After no more wire cells remain, change the powered state of all
        // them to what the powered variable ended being.
        //
        // The propagate_power() function propagates in all directions from the
        // starting point, whereas the propagate_power_to() function starts
        // only with the neighbour in the Move direction from the starting
        // point.

        template<Mob_move Move>
        void propagate_power_to(Context& context, cells16v p)
        {
                Power powered = context.table[p].powered;
                size_t n = wires.size();
                if (find_adjacent_wires<Move>(context, p)) powered = power_on;
                while (n != wires.size())
                        context.table[wires[n++]].powered = powered;
        }

        void propagate_power(Context& context, cells16v p)
        {
                Power powered = context.table[p].powered;
                size_t n = wires.size();
                if (find_adjacent_wires<Mob_move::left>(context, p)) powered = power_on;
                if (find_adjacent_wires<Mob_move::right>(context, p)) powered = power_on;
                if (find_adjacent_wires<Mob_move::up>(context, p)) powered = power_on;
                if (find_adjacent_wires<Mob_move::down>(context, p)) powered = power_on;
                if (find_adjacent_wires<Mob_move::front>(context, p)) powered = power_on;
                if (find_adjacent_wires<Mob_move::back>(context, p)) powered = power_on;
                while (n != wires.size())
                        context.table[wires[n++]].powered = powered;
        }

        // Update the state of a button cell. It is off if the cell on top of
        // it is Mob_type::none, and on otherwise.

        void update_button(Context& context, cells16v p)
        {
                auto& c = context.table[p];
                auto& c2 = context.table[p + move_traits<Mob_move::up>::direction];
                c.marked = c2.index != 0 && c2.index != ~context.player &&
                        c2.type != Mob_type::shadow_left &&
                        c2.type != Mob_type::shadow_right &&
                        c2.type != Mob_type::shadow_up &&
                        c2.type != Mob_type::shadow_down &&
                        c2.type != Mob_type::shadow_front &&
                        c2.type != Mob_type::shadow_back;
        }

        // Update the state of a plug cell. It is on if the cell on top of
        // it is Mob_type::player, and off otherwise.

        void update_plug(Context& context, cells16v p)
        {
                auto& c = context.table[p];
                c.marked =
                        context.mobs[context.player].move == Mob_move::idle &&
                        (context.table[p + move_traits<Mob_move::left>::direction].index == ~context.player ||
                         context.table[p + move_traits<Mob_move::right>::direction].index == ~context.player ||
                         context.table[p + move_traits<Mob_move::up>::direction].index == ~context.player ||
                         context.table[p + move_traits<Mob_move::down>::direction].index == ~context.player ||
                         context.table[p + move_traits<Mob_move::front>::direction].index == ~context.player ||
                         context.table[p + move_traits<Mob_move::back>::direction].index == ~context.player);
        }

        // General function to udpate logic gate states
        //
        // Checks every neighbour of the cell, except for the one at the Move
        // direction (under the assumption that it's the output) and returns a
        // pair of integers with the number of neighbours that are on or off.
        // Cells without a powered state are ignored.

        template<Mob_move Move, Mob_move Dir>
        void sample_power_impl(Context& context, const cells16v& p, int& on, int& off)
        {
                if constexpr (Move != Dir) {
                        auto& c = context.table[p + move_traits<Dir>::direction];
                        if (c.powered == power_on) ++on;
                        if (c.powered == power_off) ++off;
                }
        }

        template<Mob_move Move>
        std::pair<int, int> sample_power(Context& context, const cells16v& p)
        {
                int on = 0;
                int off = 0;
                sample_power_impl<Move, Mob_move::left>(context, p, on, off);
                sample_power_impl<Move, Mob_move::right>(context, p, on, off);
                sample_power_impl<Move, Mob_move::up>(context, p, on, off);
                sample_power_impl<Move, Mob_move::down>(context, p, on, off);
                sample_power_impl<Move, Mob_move::front>(context, p, on, off);
                sample_power_impl<Move, Mob_move::back>(context, p, on, off);
                return std::make_pair(on, off);
        }

        // Update state of all logic gates.
        //
        // The basis for every logic gate is the sample_power() function. The
        // difference between gates is what combination of inputs and outputs
        // result in the gate being powered or not:
        //
        // BUFFER: neighbour on the opposite side is ON
        // NOT: neighbour on the opposite side is not ON
        // OR: at least one neighbour ON
        // NOR: no neighbours ON
        // AND: no neighbours OFF
        // NAND: at least one neighbour OFF
        // XOR: at least one neighbour ON and one neighbour OFF
        // XNOR: no neighbours ON or no neighbours OFF

        template<Mob_move Move>
        void update_buffer(Context& context, cells16v p)
        {
                auto q = p - move_traits<Move>::direction;
                context.table[p].marked = (context.table[q].powered == power_on);
        }

        template<Mob_move Move>
        void update_not_gate(Context& context, cells16v p)
        {
                auto q = p - move_traits<Move>::direction;
                context.table[p].marked = (context.table[q].powered != power_on);
        }

        template<Mob_move Move>
        void update_or_gate(Context& context, const cells16v& p)
        {
                auto [on, off] = sample_power<Move>(context, p);
                context.table[p].marked = (on != 0);
        }

        template<Mob_move Move>
        void update_nor_gate(Context& context, const cells16v& p)
        {
                auto [on, off] = sample_power<Move>(context, p);
                context.table[p].marked = (on == 0);
        }

        template<Mob_move Move>
        void update_and_gate(Context& context, const cells16v& p)
        {
                auto [on, off] = sample_power<Move>(context, p);
                context.table[p].marked = (off == 0);
        }

        template<Mob_move Move>
        void update_nand_gate(Context& context, const cells16v& p)
        {
                auto [on, off] = sample_power<Move>(context, p);
                context.table[p].marked = (off != 0);
        }

        template<Mob_move Move>
        void update_xor_gate(Context& context, const cells16v& p)
        {
                auto [on, off] = sample_power<Move>(context, p);
                context.table[p].marked = (on != 0 && off != 0);
        }

        template<Mob_move Move>
        void update_xnor_gate(Context& context, const cells16v& p)
        {
                auto [on, off] = sample_power<Move>(context, p);
                context.table[p].marked = (on == 0 || off == 0);
        }

        // Update the state of a component.
        //
        // Doesn't touch the power flag in the cell, since that may be required
        // by a different component to calculate its own state this same tick;
        // instead, stores the new state in the "marked" field of the cell.

        void update_component_state(Context& context, cells16v p)
        {
                switch (context.table[p].type) {
                 case Mob_type::button: update_button(context, p); break;
                 case Mob_type::plug: update_plug(context, p); break;
                 case Mob_type::buffer_left: update_buffer<Mob_move::left>(context, p); break;
                 case Mob_type::buffer_right: update_buffer<Mob_move::right>(context, p); break;
                 case Mob_type::buffer_up: update_buffer<Mob_move::up>(context, p); break;
                 case Mob_type::buffer_down: update_buffer<Mob_move::down>(context, p); break;
                 case Mob_type::buffer_front: update_buffer<Mob_move::front>(context, p); break;
                 case Mob_type::buffer_back: update_buffer<Mob_move::back>(context, p); break;
                 case Mob_type::not_left: update_not_gate<Mob_move::left>(context, p); break;
                 case Mob_type::not_right: update_not_gate<Mob_move::right>(context, p); break;
                 case Mob_type::not_up: update_not_gate<Mob_move::up>(context, p); break;
                 case Mob_type::not_down: update_not_gate<Mob_move::down>(context, p); break;
                 case Mob_type::not_front: update_not_gate<Mob_move::front>(context, p); break;
                 case Mob_type::not_back: update_not_gate<Mob_move::back>(context, p); break;
                 case Mob_type::or_left: update_or_gate<Mob_move::left>(context, p); break;
                 case Mob_type::or_right: update_or_gate<Mob_move::right>(context, p); break;
                 case Mob_type::or_up: update_or_gate<Mob_move::up>(context, p); break;
                 case Mob_type::or_down: update_or_gate<Mob_move::down>(context, p); break;
                 case Mob_type::or_front: update_or_gate<Mob_move::front>(context, p); break;
                 case Mob_type::or_back: update_or_gate<Mob_move::back>(context, p); break;
                 case Mob_type::nor_left: update_nor_gate<Mob_move::left>(context, p); break;
                 case Mob_type::nor_right: update_nor_gate<Mob_move::right>(context, p); break;
                 case Mob_type::nor_up: update_nor_gate<Mob_move::up>(context, p); break;
                 case Mob_type::nor_down: update_nor_gate<Mob_move::down>(context, p); break;
                 case Mob_type::nor_front: update_nor_gate<Mob_move::front>(context, p); break;
                 case Mob_type::nor_back: update_nor_gate<Mob_move::back>(context, p); break;
                 case Mob_type::and_left: update_and_gate<Mob_move::left>(context, p); break;
                 case Mob_type::and_right: update_and_gate<Mob_move::right>(context, p); break;
                 case Mob_type::and_up: update_and_gate<Mob_move::up>(context, p); break;
                 case Mob_type::and_down: update_and_gate<Mob_move::down>(context, p); break;
                 case Mob_type::and_front: update_and_gate<Mob_move::front>(context, p); break;
                 case Mob_type::and_back: update_and_gate<Mob_move::back>(context, p); break;
                 case Mob_type::nand_left: update_nand_gate<Mob_move::left>(context, p); break;
                 case Mob_type::nand_right: update_nand_gate<Mob_move::right>(context, p); break;
                 case Mob_type::nand_up: update_nand_gate<Mob_move::up>(context, p); break;
                 case Mob_type::nand_down: update_nand_gate<Mob_move::down>(context, p); break;
                 case Mob_type::nand_front: update_nand_gate<Mob_move::front>(context, p); break;
                 case Mob_type::nand_back: update_nand_gate<Mob_move::back>(context, p); break;
                 case Mob_type::xor_left: update_xor_gate<Mob_move::left>(context, p); break;
                 case Mob_type::xor_right: update_xor_gate<Mob_move::right>(context, p); break;
                 case Mob_type::xor_up: update_xor_gate<Mob_move::up>(context, p); break;
                 case Mob_type::xor_down: update_xor_gate<Mob_move::down>(context, p); break;
                 case Mob_type::xor_front: update_xor_gate<Mob_move::front>(context, p); break;
                 case Mob_type::xor_back: update_xor_gate<Mob_move::back>(context, p); break;
                 case Mob_type::xnor_left: update_xnor_gate<Mob_move::left>(context, p); break;
                 case Mob_type::xnor_right: update_xnor_gate<Mob_move::right>(context, p); break;
                 case Mob_type::xnor_up: update_xnor_gate<Mob_move::up>(context, p); break;
                 case Mob_type::xnor_down: update_xnor_gate<Mob_move::down>(context, p); break;
                 case Mob_type::xnor_front: update_xnor_gate<Mob_move::front>(context, p); break;
                 case Mob_type::xnor_back: update_xnor_gate<Mob_move::back>(context, p); break;
                 default: break;
                }
        }

        // If the state of a component is not the same as its old state, update
        // it, then set the marked variable to true; otherwise, the marked
        // variable is set to false.

        void settle_component_state(Context& context, cells16v p)
        {
                auto& c = context.table[p];
                if (c.marked != (c.powered == power_on)) {
                        c.powered = c.marked ? power_on : power_off;
                        c.marked = true;
                        context.update_mutable = true;
                } else {
                        c.marked = false;
                }
        }

        // Propagates the state of a component to neighbouring wires.
        //
        // First, the power field is checked against the marked fields, that
        // contains the new state since update_component_state() was called. If
        // the states are the same, nothing is done; if not, the power field is
        // changed and then propagation takes place.
        //
        // Buttons propagate state to all neighbouring wires, and logic gates
        // propagate state only to a wire on the cell they're pointing at.

        void propagate_component_state(Context& context, cells16v p)
        {
                auto c = context.table[p];
                if (!c.marked) {
                        return;
                } else if (c.type == Mob_type::button) {
                        propagate_power(context, p);
                        if (c.powered == power_on) context.sounds[(size_t)Sound::press] = true;
                        else context.sounds[(size_t)Sound::release] = true;
                } else if (c.type == Mob_type::plug) {
                        propagate_power(context, p);
                        if (c.powered == power_on) context.sounds[(size_t)Sound::press] = true;
                        else context.sounds[(size_t)Sound::release] = true;
                } else if (move_traits<Mob_move::left>::is_component(c.type)) {
                        propagate_power_to<Mob_move::left>(context, p);
                } else if (move_traits<Mob_move::right>::is_component(c.type)) {
                        propagate_power_to<Mob_move::right>(context, p);
                } else if (move_traits<Mob_move::up>::is_component(c.type)) {
                        propagate_power_to<Mob_move::up>(context, p);
                } else if (move_traits<Mob_move::down>::is_component(c.type)) {
                        propagate_power_to<Mob_move::down>(context, p);
                } else if (move_traits<Mob_move::front>::is_component(c.type)) {
                        propagate_power_to<Mob_move::front>(context, p);
                } else if (move_traits<Mob_move::back>::is_component(c.type)) {
                        propagate_power_to<Mob_move::back>(context, p);
                }
        }

 public:
        // Iterate on the world table in search of circuit elements.
        //
        // For every element, set the power value of that particular cell to off
        //
        // If the element is a trigger that should be checked every frame, add
        // its index to the trigger list.

        Update_circuits(Context& context)
        {
                for (auto& c : context.table) {
                        switch (c.second.type) {
                         case Mob_type::wire:
                                c.second.powered = power_off;
                                break;
                         case Mob_type::button:
                         case Mob_type::plug:
                         case Mob_type::buffer_left:
                         case Mob_type::buffer_right:
                         case Mob_type::buffer_up:
                         case Mob_type::buffer_down:
                         case Mob_type::buffer_front:
                         case Mob_type::buffer_back:
                         case Mob_type::not_left:
                         case Mob_type::not_right:
                         case Mob_type::not_up:
                         case Mob_type::not_down:
                         case Mob_type::not_front:
                         case Mob_type::not_back:
                         case Mob_type::nor_left:
                         case Mob_type::nor_right:
                         case Mob_type::nor_up:
                         case Mob_type::nor_down:
                         case Mob_type::nor_front:
                         case Mob_type::nor_back:
                         case Mob_type::or_left:
                         case Mob_type::or_right:
                         case Mob_type::or_up:
                         case Mob_type::or_down:
                         case Mob_type::or_front:
                         case Mob_type::or_back:
                         case Mob_type::and_left:
                         case Mob_type::and_right:
                         case Mob_type::and_up:
                         case Mob_type::and_down:
                         case Mob_type::and_front:
                         case Mob_type::and_back:
                         case Mob_type::nand_left:
                         case Mob_type::nand_right:
                         case Mob_type::nand_up:
                         case Mob_type::nand_down:
                         case Mob_type::nand_front:
                         case Mob_type::nand_back:
                         case Mob_type::xor_left:
                         case Mob_type::xor_right:
                         case Mob_type::xor_up:
                         case Mob_type::xor_down:
                         case Mob_type::xor_front:
                         case Mob_type::xor_back:
                         case Mob_type::xnor_left:
                         case Mob_type::xnor_right:
                         case Mob_type::xnor_up:
                         case Mob_type::xnor_down:
                         case Mob_type::xnor_front:
                         case Mob_type::xnor_back:
                                triggers.push_back(c.first);
                                c.second.powered = power_off;
                                break;
                         default:
                                break;
                        }
                }
        }

        // Update circuits
        //
        // First check every trigger and see if its powered status changed from
        // the last tick. If so, propagate that state through all connected
        // wires.

        void operator()(Context& context)
        {
                if (context.tick % 2)
                        return;
                for (const auto& p : triggers)
                        update_component_state(context, p);
                for (const auto& p : triggers)
                        settle_component_state(context, p);
                for (const auto& p : triggers)
                        propagate_component_state(context, p);
                for (const auto& p : wires)
                        context.table[p].marked = false;
                wires.clear();
        }

};

#endif // SRC_CIRCUIT_H_
