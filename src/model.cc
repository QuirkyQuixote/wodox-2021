// model.cc - load model database

// Copyright (C) 2021 Luis Sanz <luis.sanz@gmail.com>

// This file is part of Wodox 2021.

// Wodox 2021 is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Wodox 2021 is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with Wodox 2021.  If not, see <https://www.gnu.org/licenses/>.


#include "model.h"

#include <glm/gtc/matrix_transform.hpp>

#include "media.h"
#include "mdea/mdeaxx.h"
#include "xdgbds.h"
#include "context.h"

struct Model_loader {
        Model_data& data;

        Model_loader(Model_data& data) : data{data} {}

        void operator()(const std::filesystem::path& path)
        {
                if (auto doc = mdea::document{path}) {
                        parse(doc.root().get_object());
                        return;
                }
                throw std::runtime_error{path.string() + ": bad JSON"};
        }

        void parse(const mdea::object& obj)
        {
                Variant v;
                size_t first_vertex = data.vertices.size();
                v.first_face = data.faces.size();
                for (auto x : obj["faces"].get_array())
                        parse_face(x.get_object());
                v.last_face = data.faces.size();
                calculate_vectors(first_vertex, data.vertices.size());
                if (auto x = obj["rotation"]) {
                        auto rot = parse_rotation(x.get_array());
                        rotate_vertices(first_vertex, data.vertices.size(), rot);
                        rotate_faces(v.first_face, data.faces.size(), rot);
                }
                data.variants.push_back(v);
        }

        void parse_face(const mdea::object& obj)
        {
                Face f;
                if (auto x = obj["cull"])
                        f.cull = parse_cull(x.get_string());
                else
                        f.cull = Cull::none;
                f.first_vertex = data.vertices.size();
                parse_vertices(obj["vertices"].get_array(),
                                parse_textureid(obj["textureid"].get_long()));
                f.last_vertex = data.vertices.size();
                data.faces.push_back(f);
        }

        glm::vec2 parse_textureid(int n)
        {
                return glm::vec2((n % 16) / 16.f, (n / 16) / 20.f);
        }

        void parse_vertices(const mdea::array& arr, const glm::vec2& texcoord)
        {
                for (auto x : arr) {
                        Vertex v;
                        v.position = parse_vertex(x.get_array());
                        v.texcoord = texcoord;
                        data.vertices.push_back(v);
                }
        }

        glm::vec3 parse_vertex(const mdea::array& arr)
        {
                if (arr.size() != 3)
                        throw std::runtime_error{"vertex definition must have three elements"};
                glm::vec3 v{
                        arr[0].get_float(),
                        arr[1].get_float(),
                        arr[2].get_float(),
                };
                return v;
        }

        glm::ivec3 parse_rotation(const mdea::array& arr)
        {
                if (arr.size() != 3)
                        throw std::runtime_error{"model rotation must have three elements"};
                glm::ivec3 v {
                        arr[0].get_long(),
                        arr[1].get_long(),
                        arr[2].get_long()
                };
                if (v.x < 0 || v.x > 3 || v.y < 0 || v.y > 3 || v.z < 0 || v.z > 3)
                        throw std::runtime_error{"each component of model rotation must be between 0 and 3"};
                return v;
        }

        Cull parse_cull(const std::string_view& str)
        {
                return str == "left" ? Cull::left :
                        str == "right" ? Cull::right :
                        str == "up" ? Cull::up :
                        str == "down" ? Cull::down :
                        str == "front" ? Cull::front :
                        str == "back" ? Cull::back :
                        Cull::none;
        }

        Cull normal_direction(const glm::vec3& normal)
        {
                float x = abs(normal.x);
                float y = abs(normal.y);
                float z = abs(normal.z);
                if (x > y) {
                        if (x > z) {
                                return normal.x > 0 ? Cull::right : Cull::left;
                        } else {
                                return normal.y > 0 ? Cull::back : Cull::front;
                        }
                } else {
                        if (y > z) {
                                return normal.y > 0 ? Cull::down : Cull::up;
                        } else {
                                return normal.y > 0 ? Cull::back : Cull::front;
                        }
                }
        }

        void calculate_texcoords(size_t i, Cull direction)
        {
                switch (direction) {
                 case Cull::left:
                        for (size_t j = i; j < i + 3; ++j) {
                                data.vertices[j].texcoord.x += data.vertices[j].position.z / 16.f;
                                data.vertices[j].texcoord.y += (1 - data.vertices[j].position.y) / 20.f;
                        }
                        break;
                 case Cull::right:
                        for (size_t j = i; j < i + 3; ++j) {
                                data.vertices[j].texcoord.x += (1 - data.vertices[j].position.z) / 16.f;
                                data.vertices[j].texcoord.y += (1 - data.vertices[j].position.y) / 20.f;
                        }
                        break;
                 case Cull::up:
                        for (size_t j = i; j < i + 3; ++j) {
                                data.vertices[j].texcoord.x += data.vertices[j].position.x / 16.f;
                                data.vertices[j].texcoord.y += data.vertices[j].position.z / 20.f;
                        }
                        break;
                 case Cull::down:
                        for (size_t j = i; j < i + 3; ++j) {
                                data.vertices[j].texcoord.x += (1 - data.vertices[j].position.x) / 16.f;
                                data.vertices[j].texcoord.y += data.vertices[j].position.z / 20.f;
                        }
                        break;
                 case Cull::front:
                        for (size_t j = i; j < i + 3; ++j) {
                                data.vertices[j].texcoord.x += data.vertices[j].position.x / 16.f;
                                data.vertices[j].texcoord.y += (1 - data.vertices[j].position.y) / 20.f;
                        }
                        break;
                 case Cull::back:
                        for (size_t j = i; j < i + 3; ++j) {
                                data.vertices[j].texcoord.x += (1 - data.vertices[j].position.x) / 16.f;
                                data.vertices[j].texcoord.y += (1 - data.vertices[j].position.y) / 20.f;
                        }
                        break;
                 default:
                        break;
                }
        }

        void calculate_vectors(size_t first, size_t last)
        {
                for (size_t i = first; i != last; i += 3) {
                        auto a = data.vertices[i + 1].position - data.vertices[i].position;
                        auto b = data.vertices[i + 2].position - data.vertices[i].position;
                        auto normal = glm::normalize(glm::cross(a, b));
                        calculate_texcoords(i, normal_direction(normal));
                        auto c = data.vertices[i + 1].texcoord - data.vertices[i].texcoord;
                        auto d = data.vertices[i + 2].texcoord - data.vertices[i].texcoord;
                        float f = 1.0f / (c.x * d.y - d.x * c.y);
                        glm::vec3 tangent{f * (d.y * a.x - c.y * b.x),
                                  f * (d.y * a.y - c.y * b.y),
                                  f * (d.y * a.z - c.y * b.z)
                        };
                        glm::vec3 bitangent{f * (-d.x * a.x + c.x * b.x),
                                f * (-d.x * a.y + c.x * b.y),
                                f * (-d.x * a.z + c.x * b.z)
                        };
                        data.vertices[i].normal = normal;
                        data.vertices[i + 1].normal = normal;
                        data.vertices[i + 2].normal = normal;
                        data.vertices[i].tangent = tangent;
                        data.vertices[i + 1].tangent = tangent;
                        data.vertices[i + 2].tangent = tangent;
                        data.vertices[i].bitangent = bitangent;
                        data.vertices[i + 1].bitangent = bitangent;
                        data.vertices[i + 2].bitangent = bitangent;
                }
        }

        void rotate_vertices(size_t first, size_t last, const glm::ivec3& rot)
        {
                static const glm::vec3 offset{0.5, 0.5, 0.5};
                float x = glm::half_pi<float>() * rot.x;
                float y = glm::half_pi<float>() * rot.y;
                float z = glm::half_pi<float>() * rot.z;
                glm::mat4 ret{1.f};
                ret = glm::rotate(ret, z, glm::vec3{0.f, 0.f, 1.f});
                ret = glm::rotate(ret, y, glm::vec3{0.f, 1.f, 0.f});
                ret = glm::rotate(ret, x, glm::vec3{1.f, 0.f, 0.f});
                glm::mat3 mat{ret};
                for (size_t i = first; i != last; ++i) {
                        data.vertices[i].position = mat * (data.vertices[i].position - offset) + offset;
                        data.vertices[i].normal = mat * data.vertices[i].normal;
                        data.vertices[i].tangent = mat * data.vertices[i].tangent;
                        data.vertices[i].bitangent = mat * data.vertices[i].bitangent;
                }
        }

        void rotate_faces(size_t first, size_t last, const glm::ivec3& rot)
        {
                int z_rotation[4][7] = {
                        { 0, 1, 2, 3, 4, 5, 6 },
                        { 0, 3, 4, 2, 1, 5, 6 },
                        { 0, 2, 1, 4, 3, 5, 6 },
                        { 0, 4, 3, 1, 2, 5, 6 },
                };
                int y_rotation[4][7] = {
                        { 0, 1, 2, 3, 4, 5, 6 },
                        { 0, 5, 6, 3, 4, 1, 2 },
                        { 0, 2, 1, 3, 4, 6, 5 },
                        { 0, 6, 5, 3, 4, 2, 1 },
                };
                int x_rotation[4][7] = {
                        { 0, 1, 2, 3, 4, 5, 6 },
                        { 0, 1, 2, 5, 6, 4, 3 },
                        { 0, 1, 2, 4, 3, 6, 5 },
                        { 0, 1, 2, 6, 5, 3, 4 },
                };
                for (size_t i = first; i != last; ++i) {
                        int cull = static_cast<int>(data.faces[i].cull);
                        cull = z_rotation[rot.z][cull];
                        cull = y_rotation[rot.y][cull];
                        cull = x_rotation[rot.x][cull];
                        data.faces[i].cull = static_cast<Cull>(cull);
                }
        }
};

void load_model(const std::filesystem::path& path, Model_data& data)
{
        Model_loader load{data};
        load(path);
}

struct Model_data_loader {
        Model_data data;

        Model_data operator()(const std::filesystem::path& path)
        {
                if (auto doc = mdea::document{path}) {
                        parse(doc.root().get_object());
                        return std::move(data);
                }
                throw std::runtime_error{path.string() + ": bad JSON"};
        }

        void parse(const mdea::object& obj)
        {
                for (size_t i = 0; i < (size_t)Mob_type::count; ++i)
                        parse_model(obj[mob_type_name((Mob_type)i)].get_array());
        }

        void parse_model(const mdea::array& arr)
        {
                std::filesystem::path basedir{BASEDIR};
                Model m;
                m.first_variant = data.variants.size();
                for (auto x : arr)
                        load_model(xdg::data::find(basedir / x.get_string()), data);
                m.last_variant = data.variants.size();
                data.models.push_back(m);
        }
};

Model_data load_model_data(const std::filesystem::path& path)
{
        Model_data_loader load;
        return load(path);
}
