// model.h - declare model database

// Copyright (C) 2021 Luis Sanz <luis.sanz@gmail.com>

// This file is part of Wodox 2021.

// Wodox 2021 is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Wodox 2021 is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with Wodox 2021.  If not, see <https://www.gnu.org/licenses/>.


#ifndef SRC_MODEL_H_
#define SRC_MODEL_H_

#include <vector>
#include <filesystem>

#include <glm/glm.hpp>

enum class Cull { none, left, right, down, up, front, back };

struct Vertex {
        glm::vec3 position;
        glm::vec3 normal;
        glm::vec2 texcoord;
        glm::vec3 tangent;
        glm::vec3 bitangent;
};

struct Face {
        int first_vertex;
        int last_vertex;
        Cull cull;
};

struct Variant {
        int first_face;
        int last_face;
};

struct Model {
        int first_variant;
        int last_variant;
};

struct Model_data {
        std::vector<Vertex> vertices;
        std::vector<Face> faces;
        std::vector<Variant> variants;
        std::vector<Model> models;
};

Model_data load_model_data(const std::filesystem::path& path);

#endif // SRC_MODEL_H_
