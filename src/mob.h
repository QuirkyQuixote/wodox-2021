// mob.h - implement mobs

// Copyright (C) 2021 Luis Sanz <luis.sanz@gmail.com>

// This file is part of Wodox 2021.

// Wodox 2021 is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Wodox 2021 is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with Wodox 2021.  If not, see <https://www.gnu.org/licenses/>.


#ifndef SRC_MOB_H_
#define SRC_MOB_H_

#include "context.h"

class Update_mobs {
 private:
        cells16b dead_box;

        // Use for debugging if things go haywire.

        void assert_mobs_in_place(Context& context)
        {
                for (auto it = context.table.begin(); it != context.table.end(); ++it) {
                        if (it->second.index < 0) {
                                auto& m = context.mobs[~it->second.index];
                                assert(m.pos == it->first);
                        }
                }
        }

        // Check if a mob can fall in the direction of the local gravity.
        // This function is called for mobs in the settle state, including:
        //
        //  - all mobs when the level is loaded
        //  - mobs that have completed an horizontal move
        //  - mobs that have had a shadow cell removed from under or over them

        bool is_shakeable(Mob_type type)
        {
                return type == Mob_type::player || type == Mob_type::crate;
        }

        template<Mob_move Move> bool can_shake_mob_impl(Context& context, Mob& mob)
        {
                auto& c = context.table[mob.pos + move_traits<Move>::direction];
                if (c.type == Mob_type::none || c.type == move_traits<Move>::shadow)
                        return true;
                if (mob.alive)
                        return false;
                if (c.type == Mob_type::shadow_left) {
                        auto c2 = context.table[mob.pos + move_traits<Mob_move::left>::direction];
                        return c2.type == Mob_type::none ||
                                c.type == Mob_type::shadow_left;
                }
                if (c.type == Mob_type::shadow_right) {
                        auto c2 = context.table[mob.pos + move_traits<Mob_move::right>::direction];
                        return c2.type == Mob_type::none ||
                                c.type == Mob_type::shadow_right;
                }
                if (c.type == Mob_type::shadow_front) {
                        auto c2 = context.table[mob.pos + move_traits<Mob_move::front>::direction];
                        return c2.type == Mob_type::none ||
                                c.type == Mob_type::shadow_front;
                }
                if (c.type == Mob_type::shadow_back) {
                        auto c2 = context.table[mob.pos + move_traits<Mob_move::back>::direction];
                        return c2.type == Mob_type::none ||
                                c.type == Mob_type::shadow_back;
                }
                return false;
        }

        bool can_shake_mob(Context& context, Mob& mob)
        {
                if (!is_shakeable(mob.type)) return false;
                Gravity gravity = context.table[mob.pos].gravity;
                if (gravity == downwards_gravity) {
                        if (can_shake_mob_impl<Mob_move::down>(context, mob))
                                return true;
                }
                if (gravity == upwards_gravity) {
                        if (can_shake_mob_impl<Mob_move::up>(context, mob))
                                return true;
                }
                mob.speed = 0;
                mob.frac = 0;
                return false;
        }

        // Make a mob fall in the direction of the local gravity.
        // Assumes that the movement is possible, as checked by can_shake_mob().

        template<Mob_move Move> void shake_mob_to(Context& context, Mob& mob)
        {
                auto it = context.table.find(mob.pos);
                auto next = context.table.find(mob.pos + move_traits<Move>::direction);
                next->second.index = it->second.index;
                it->second.type = move_traits<Move>::shadow;
                mob.pos += move_traits<Move>::direction;
                mob.move = Move;
                mob.frac += 64;
                if constexpr (Move != Mob_move::down) mob.speed = 8;
                else mob.falling = true;
        }

        template<Mob_move Move> void shake_mob_impl(Context& context, Mob& mob)
        {
                Mob_type type = context.table[mob.pos + move_traits<Move>::direction].type;
                if (type == Mob_type::none || type == move_traits<Move>::shadow)
                        shake_mob_to<Move>(context, mob);
                else if (type == Mob_type::shadow_left)
                        shake_mob_to<Mob_move::left>(context, mob);
                else if (type == Mob_type::shadow_right)
                        shake_mob_to<Mob_move::right>(context, mob);
                else if (type == Mob_type::shadow_front)
                        shake_mob_to<Mob_move::front>(context, mob);
                else if (type == Mob_type::shadow_back)
                        shake_mob_to<Mob_move::back>(context, mob);
        }

        void shake_mob(Context& context, Mob& mob)
        {
                Gravity gravity = context.table[mob.pos].gravity;
                if (gravity == downwards_gravity)
                        shake_mob_impl<Mob_move::down>(context, mob);
                else if (gravity == upwards_gravity)
                        shake_mob_impl<Mob_move::up>(context, mob);
        }

        // Check if there's a mob in the given position, and if so, try make it fall.

        void wakeup_mob(Context& context, const cells16v& p)
        {
                auto& c = context.table[p];
                if (c.index >= 0) return;
                Mob& mob = context.mobs[~c.index];
                if (mob.move != Mob_move::idle) return;
                mob.move = Mob_move::settle;
        }

        // Erase the trail marker in the table for a mob that completed its
        // movement, unless the trail has already been removed by a mob moving
        // into the old position.
        //
        // If the marker is removed and movement was not vertical, call
        // wakeup_mob for the position on top of the marker that was just
        // removed.

        template<Mob_move Move>
        void erase_trail(Context& context, Mob& mob)
        {
                auto p = mob.pos - move_traits<Move>::direction;
                auto& c = context.table[p];
                if (c.type != move_traits<Move>::shadow) return;
                c.type = Mob_type::none;
                if constexpr (Move != Mob_move::up)
                        wakeup_mob(context, p + move_traits<Mob_move::up>::direction);
                if constexpr (Move != Mob_move::down)
                        wakeup_mob(context, p + move_traits<Mob_move::down>::direction);
        }

        // Cleanup mob data at the end of movement for a particular direction
        //
        // - First, try to erase the trail object behind the mob.
        // - If the mob is set up to keep moving, end here for the resume stage.
        // - Otherwise, set it down for settling so gravity will have a hold on
        //   it in the settling stage.

        template<Mob_move Move>
        void end_move_mob_impl(Context& context, Mob& mob)
        {
                erase_trail<Move>(context, mob);
                if (!geom::contains(dead_box, mob.pos)) {
                        context.table[mob.pos].type = Mob_type::none;
                        mob.type = Mob_type::none;
                } else {
                        mob.move = Mob_move::settle;
                }
        }

        void end_move_mob(Context& context, Mob& mob)
        {
                if (mob.move == Mob_move::left)
                        end_move_mob_impl<Mob_move::left>(context, mob);
                else if (mob.move == Mob_move::right)
                        end_move_mob_impl<Mob_move::right>(context, mob);
                else if (mob.move == Mob_move::up)
                        end_move_mob_impl<Mob_move::up>(context, mob);
                else if (mob.move == Mob_move::down)
                        end_move_mob_impl<Mob_move::down>(context, mob);
                else if (mob.move == Mob_move::front)
                        end_move_mob_impl<Mob_move::front>(context, mob);
                else if (mob.move == Mob_move::back)
                        end_move_mob_impl<Mob_move::back>(context, mob);
        }

        // Called by mobs that start moving to drag other mobs that stand on top of
        // them. It will start a new mob moving chain.

        template<Mob_move Move>
        void drag_mobs(Context& context, Mob& mob, uint8_t speed)
        {
                if constexpr (Move == Mob_move::up || Move == Mob_move::down) return;
                for (auto pos = mob.pos;;) {
                        pos += move_traits<Mob_move::up>::direction;
                        auto& c = context.table[pos];
                        if (c.gravity == upwards_gravity) break;
                        if (c.index >= 0) break;
                        auto& mob = context.mobs[~c.index];
                        if (mob.alive) break;
                        if (!is_shakeable(mob.type)) break;
                        if (mob.move != Mob_move::idle && mob.move != Mob_move::settle) break;
                        auto next = pos + move_traits<Move>::direction;
                        auto& c2 = context.table[next];
                        if (c2.type != Mob_type::none && c2.type != move_traits<Move>::shadow) break;
                        mob.pos = next;
                        mob.move = Move;
                        mob.speed = speed;
                        mob.frac = 64;
                        c2.index = c.index;
                        c.type = move_traits<Move>::shadow;
                }
                for (auto pos = mob.pos;;) {
                        pos += move_traits<Mob_move::down>::direction;
                        auto& c = context.table[pos];
                        if (c.gravity == downwards_gravity) break;
                        if (c.index >= 0) break;
                        auto& mob = context.mobs[~c.index];
                        if (mob.alive) break;
                        if (!is_shakeable(mob.type)) break;
                        if (mob.move != Mob_move::idle && mob.move != Mob_move::settle) break;
                        auto next = pos + move_traits<Move>::direction;
                        auto& c2 = context.table[next];
                        if (c2.type != Mob_type::none && c2.type != move_traits<Move>::shadow) break;
                        mob.pos = next;
                        mob.move = Move;
                        mob.speed = speed;
                        mob.frac = 64;
                        c2.index = c.index;
                        c.type = move_traits<Move>::shadow;
                }
        }

        // Return true if a mob can be moved in the given direction. If other
        // mobs are on the way, the function will check if they can be moved
        // too.

        template<Mob_move Move>
        bool can_move_mob(Context& context, Mob& mob)
        {
                if (mob.move != Mob_move::idle && mob.move != Mob_move::settle) return false;
                auto pos = mob.pos;
                for (;;) {
                        pos += move_traits<Move>::direction;
                        auto& c = context.table[pos];
                        if (c.type == Mob_type::none || c.type == move_traits<Move>::shadow) return true;
                        if (c.index > 0) return false;
                        auto& mob2 = context.mobs[~c.index];
                        if (mob2.move != Mob_move::idle && mob2.move != Mob_move::settle) return false;
                        if (!is_shakeable(mob2.type)) return false;
                }
                return false;
        }

        // Actually start moving a mob in the given direction, with the given
        // speed.  Assumes movement is possible, as checked by can_move_mob
        // It will push other mobs in front of the initial one
        // It will recursively drag mobs pressing on the mob row by gravity.

        template<Mob_move Move>
        void move_mob(Context& context, Mob& mob, uint8_t speed)
        {
                auto first = mob.pos;
                auto last = first;
                for (;;) {
                        last += move_traits<Move>::direction;
                        auto& c = context.table[last];
                        if (c.index >= 0) break;
                        auto& mob = context.mobs[~c.index];
                        if (mob.move != Mob_move::idle && mob.move != Mob_move::settle) break;
                }
                while (last != first) {
                        auto next = last - move_traits<Move>::direction;
                        auto& c = context.table[next];
                        auto& c2 = context.table[last];
                        auto& mob = context.mobs[~c.index];
                        c2.index = c.index;
                        drag_mobs<Move>(context, mob, speed);
                        mob.pos = last;
                        mob.move = Move;
                        mob.speed = speed;
                        mob.frac = 64;
                        last = next;
                }
                context.table.find(first)->second.type = move_traits<Move>::shadow;
        }

        // Call can_move_mob, and if true, call move_mob.

        template<Mob_move Move>
        bool try_move_mob(Context& context, Mob& mob, uint8_t speed)
        {
                if (can_move_mob<Move>(context, mob)) {
                        move_mob<Move>(context, mob, speed);
                        return true;
                }
                return false;
        }

        // Check if a cell adjacent to an engine in a particular direction is
        // powered, and if so launch the engine in the opposite direction

        template<Mob_move Move>
        bool try_trigger_engine_impl(Context& context, Mob& mob)
        {
                auto& c = context.table[mob.pos - move_traits<Move>::direction];
                if (c.powered != power_on) return false;
                if (!try_move_mob<Move>(context, mob, 8)) return false;
                mob.type = move_traits<Move>::engine;
                return true;
        }

        bool try_trigger_engine(Context& context, Mob& mob)
        {
                return try_trigger_engine_impl<Mob_move::left>(context, mob) ||
                        try_trigger_engine_impl<Mob_move::right>(context, mob) ||
                        try_trigger_engine_impl<Mob_move::up>(context, mob) ||
                        try_trigger_engine_impl<Mob_move::down>(context, mob) ||
                        try_trigger_engine_impl<Mob_move::front>(context, mob) ||
                        try_trigger_engine_impl<Mob_move::back>(context, mob);
        }

        // Moving engines that completed their movement have one more chance to
        // keep moving, otherwise they become unmoving engines again.

        void try_move_again(Context& context, Mob& mob)
        {
                if (mob.frac > 0) {
                        return;
                } else if (mob.type == Mob_type::engine_left) {
                        try_move_mob<Mob_move::left>(context, mob, 8) || try_trigger_engine(context, mob);
                } else if (mob.type == Mob_type::engine_right) {
                        try_move_mob<Mob_move::right>(context, mob, 8) || try_trigger_engine(context, mob);
                } else if (mob.type == Mob_type::engine_up) {
                        try_move_mob<Mob_move::up>(context, mob, 8) || try_trigger_engine(context, mob);
                } else if (mob.type == Mob_type::engine_down) {
                        try_move_mob<Mob_move::down>(context, mob, 8) || try_trigger_engine(context, mob);
                } else if (mob.type == Mob_type::engine_front) {
                        try_move_mob<Mob_move::front>(context, mob, 8) || try_trigger_engine(context, mob);
                } else if (mob.type == Mob_type::engine_back) {
                        try_move_mob<Mob_move::back>(context, mob, 8) || try_trigger_engine(context, mob);
                }
        }

        template<Mob_move Move> void update_player_impl(Context& context)
        {
                auto& mob = context.mobs[context.player];
                if (mob.move != Mob_move::idle) return;
                context.sounds[(size_t)Sound::player] = try_move_mob<Move>(context, mob, 8);
        }

        void update_player(Context& context, int player_dir)
        {
                context.mobs[context.player].alive = (player_dir != -1);
                switch (player_dir) {
                 case 0: update_player_impl<Mob_move::left>(context); break;
                 case 1: update_player_impl<Mob_move::front>(context); break;
                 case 2: update_player_impl<Mob_move::right>(context); break;
                 case 3: update_player_impl<Mob_move::back>(context); break;
                }
        }

        void settle_mobs(Context& context)
        {
                for (auto& mob : context.mobs)
                        if (mob.move == Mob_move::settle)
                                mob.move = can_shake_mob(context, mob) ?
                                        Mob_move::shake : Mob_move::idle;
        }

        void shake_mobs(Context& context)
        {
                for (auto& mob : context.mobs)
                        if (mob.move == Mob_move::shake)
                                shake_mob(context, mob);
        }

        void update_engines(Context& context)
        {
                for (auto& mob : context.mobs)
                        try_move_again(context, mob);
        }

        void update_moving(Context& context)
        {
                for (auto& mob : context.mobs) {
                        if (mob.type == Mob_type::none)
                                continue;
                        if (mob.falling && mob.move != Mob_move::down) {
                                context.sounds[(size_t)Sound::land] = true;
                                mob.falling = false;
                        }
                        if (mob.move == Mob_move::idle)
                                continue;
                        mob.frac -= mob.speed;
                        if (mob.frac <= 0)
                                end_move_mob(context, mob);
                        else if (mob.falling)
                                ++mob.speed;
                }
        }

 public:
        // Populate the mob list from from values in the world table.
        // The cells that generated mobs will be rewritten to contain the
        // indices of the generated mobs.
        //
        // Also generate areas of different gravity from lift and hover cells.
        // The cells converted in such way will become empty.

        Update_mobs(Context& context)
        {
                for (auto& c : context.table) {
                        if (c.second.type == Mob_type::player ||
                                        c.second.type == Mob_type::crate ||
                                        c.second.type == Mob_type::engine_left ||
                                        c.second.type == Mob_type::engine_right ||
                                        c.second.type == Mob_type::engine_up ||
                                        c.second.type == Mob_type::engine_down ||
                                        c.second.type == Mob_type::engine_front ||
                                        c.second.type == Mob_type::engine_back) {
                                int8_t index = context.mobs.size();
                                Mob_type type = c.second.type;
                                context.mobs.push_back(Mob{c.first, type, Mob_move::settle});
                                c.second.index = ~index;
                                if (type == Mob_type::player) context.player = index;
                        } else if (c.second.type == Mob_type::lift) {
                                c.second.gravity = upwards_gravity;
                                c.second.type = Mob_type::none;
                        } else if (c.second.type == Mob_type::hover) {
                                c.second.gravity = no_gravity;
                                c.second.type = Mob_type::none;
                        }
                }

                //dead_box = cells16b(cells16v(1, 1, 1), context.box.size() - cells16v(2, 2, 2));
                dead_box = cells16b(context.box.min() - 1_cl, context.box.max() + 1_cl);
        }

        // For every mob in the settle state, check if it can move in the
        // direction of the local gravity and if so set it to the shake state.
        // This movement does not involve pushing other mobs. Mobs that can't
        // shake become idle.
        //
        // For every mob in the shake state, move them in the direction of the
        // local gravity and change their state to the appropriate move state.  
        //
        // Update the movement of all mobs that are not in an idle state,
        // update movement fraction and speed. If the fraction reaches zero,
        // complete the movement and erase all trails, switching the mob and
        // all surrounding one to the settle state.
        //
        // The player mob moves according to the player_dir variable

        void operator()(Context& context, int player_dir)
        {
                settle_mobs(context);
                shake_mobs(context);
                update_engines(context);
                update_player(context, player_dir);
                update_moving(context);
        }
};

#endif // SRC_MOB_H_
