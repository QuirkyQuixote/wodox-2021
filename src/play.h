// play.h - primary game logic

// Copyright (C) 2021 Luis Sanz <luis.sanz@gmail.com>

// This file is part of Wodox 2021.

// Wodox 2021 is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Wodox 2021 is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with Wodox 2021.  If not, see <https://www.gnu.org/licenses/>.


#ifndef SRC_GAME_H_
#define SRC_GAME_H_

#include "media.h"
#include "render.h"
#include "config.h"
#include "mob.h"
#include "circuit.h"

struct Input {
        bool up : 1;
        bool down : 1;
        bool left : 1;
        bool right : 1;
        bool ccw : 1;
        bool cw : 1;
        bool quit : 1;
};

class Audio {
 private:
        Subsystem_lock<SDL_INIT_AUDIO> slock;
        std::vector<Chunk> chunks;
        Music music;

        void update_music(Context& context);
        void update_sounds(Context& context);

 public:
        Audio(const Audio_config& config);
        ~Audio();
        void update(Context& context);
};

// Camera

class Camera {
 private:
        int angle_ = 0;
        glm::vec3 offset{0.f, 0.f, 0.f};
        glm::vec3 target;
        glm::vec3 position;
        Mvp mvp_;

        void update_vectors(Context& context);
        void update_mvp();

 public:
        void update(Context& context);
        int angle() const { return angle_; }
        void angle(int new_angle) { angle_ = new_angle; }
        const Mvp& mvp() const { return mvp_; }
};

class Handle_events {
 private:
        Subsystem_lock<SDL_INIT_GAMECONTROLLER> slock;
        const Game_input_config& config;
        std::vector<Gamepad> gamepads;

        void handle_event(const SDL_Event& event, Input& input);

 public:
        Handle_events(const Game_input_config& config);
        void operator()(Input& input);
};

class Update : public Main_loop<Update> {
 private:
        Input input{0};
        Context context;
        Context checkpoint;
        Renderer renderer;
        Audio audio;
        Camera camera;
        Handle_events handle_events;
        Update_mobs update_mobs;
        Update_circuits update_circuits;

        void update_level();
        int update_player();
        void update_camera();

 public:
        Update(const Config& config, const std::filesystem::path& path);
        bool update();
};

#endif // SRC_GAME_H_
