// context.h - declare Context and related functions

// Copyright (C) 2021 Luis Sanz <luis.sanz@gmail.com>

// This file is part of Wodox 2021.

// Wodox 2021 is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Wodox 2021 is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with Wodox 2021.  If not, see <https://www.gnu.org/licenses/>.


#ifndef SRC_CONTEXT_H_
#define SRC_CONTEXT_H_

#include <bitset>
#include <unordered_map>

#include "geom/num.h"
#include "geom/math.h"
#include "geom/packed_table.h"

#include "media.h"

// table space is used to address the world table, and gives the actual
// positions of the mobs, both for game physics and rendering.

using cells8 = geom::Num<int8_t, geom::Unit<1>, std::ratio<1>>;
using cells8v = geom::Vec<cells8, 3>;
using cells8b = geom::Box<cells8, 3>;

using cells16 = geom::Num<int16_t, geom::Unit<1>, std::ratio<1>>;
using cells16v = geom::Vec<cells16, 3>;
using cells16b = geom::Box<cells16, 3>;

using cells32 = geom::Num<int32_t, geom::Unit<1>, std::ratio<1>>;
using cells32v = geom::Vec<cells32, 3>;
using cells32b = geom::Box<cells32, 3>;

constexpr cells32 operator ""_cl(unsigned long long n) { return cells32(n); }

// Possible states for a mob

enum class Mob_move : int8_t {
        idle,   // asleep and resting; requires update to move again
        left,   // moving left
        right,  // moving right
        up,     // moving up
        down,   // moving down
        front,  // moving front
        back,   // moving back
        settle, // completed a move, and waiting to see the next state
        shake,  // completed a move, and waiting to be affected by gravity
};

// All possible mob types.

enum class Mob_type : int8_t {
        none, fixed, player, crate, lift, hover, button, wire, checkpoint, goal, plug,
        buffer_left, buffer_right, buffer_up, buffer_down, buffer_front, buffer_back,
        not_left, not_right, not_up, not_down, not_front, not_back,
        or_left, or_right, or_up, or_down, or_front, or_back,
        nor_left, nor_right, nor_up, nor_down, nor_front, nor_back,
        and_left, and_right, and_up, and_down, and_front, and_back,
        nand_left, nand_right, nand_up, nand_down, nand_front, nand_back,
        xor_left, xor_right, xor_up, xor_down, xor_front, xor_back,
        xnor_left, xnor_right, xnor_up, xnor_down, xnor_front, xnor_back,
        engine_left, engine_right, engine_up, engine_down, engine_front, engine_back,
        shadow_left, shadow_right, shadow_up, shadow_down, shadow_front, shadow_back,
        cursor, cursor_2,
        count,
};

// Directions of gravity.
// Can't be an enum class because it'll become a two-bit field in the Cell.

enum Gravity {
        downwards_gravity, upwards_gravity, no_gravity
};

// Possible powered values for a cell (not part of a circuit/off/on)
// Can't be an enum class because it'll become a two-bit field in the Cell.

enum Power {
        no_power, power_off, power_on
};

// Indices of all the possible sounds that can be played

enum class Sound {
        player,
        press,
        release,
        land,
        camera,
        write,
        flip,
};

// Traits for Mob_move values

template<Mob_move Move> struct move_traits { };

template<> struct move_traits<Mob_move::idle> {
        constexpr static const cells16v direction{0, 0, 0};
        constexpr static const char* name = "idle";
        constexpr static const Mob_move reverse = Mob_move::idle;
        constexpr static const Mob_type shadow = Mob_type::none;
        constexpr static const Mob_type engine = Mob_type::none;
        constexpr static const Mob_type buffer = Mob_type::none;
        constexpr static const Mob_type not_gate = Mob_type::none;
        constexpr static const Mob_type or_gate = Mob_type::none;
        constexpr static const Mob_type nor_gate = Mob_type::none;
        constexpr static const Mob_type and_gate = Mob_type::none;
        constexpr static const Mob_type nand_gate = Mob_type::none;
        constexpr static const Mob_type xor_gate = Mob_type::none;
        constexpr static const Mob_type xnor_gate = Mob_type::none;
        constexpr static bool is_component(Mob_type type) { return false; }
};

template<> struct move_traits<Mob_move::left> {
        constexpr static const cells16v direction{-1, 0, 0};
        constexpr static const char* name = "left";
        constexpr static const Mob_move reverse = Mob_move::right;
        constexpr static const Mob_type shadow = Mob_type::shadow_left;
        constexpr static const Mob_type engine = Mob_type::engine_left;
        constexpr static const Mob_type buffer = Mob_type::buffer_left;
        constexpr static const Mob_type not_gate = Mob_type::not_left;
        constexpr static const Mob_type or_gate = Mob_type::or_left;
        constexpr static const Mob_type nor_gate = Mob_type::nor_left;
        constexpr static const Mob_type and_gate = Mob_type::and_left;
        constexpr static const Mob_type nand_gate = Mob_type::nand_left;
        constexpr static const Mob_type xor_gate = Mob_type::xor_left;
        constexpr static const Mob_type xnor_gate = Mob_type::xnor_left;
        constexpr static bool is_component(Mob_type type)
        {
                return type == Mob_type::button || type == Mob_type::plug ||
                        type == buffer || type == not_gate ||
                        type == or_gate || type == nor_gate || type == and_gate ||
                        type == nand_gate || type == xor_gate || type == xnor_gate;
        }
};

template<> struct move_traits<Mob_move::right> {
        constexpr static const cells16v direction{1, 0, 0};
        constexpr static const char* name = "right";
        constexpr static const Mob_move reverse = Mob_move::left;
        constexpr static const Mob_type shadow = Mob_type::shadow_right;
        constexpr static const Mob_type engine = Mob_type::engine_right;
        constexpr static const Mob_type buffer = Mob_type::buffer_right;
        constexpr static const Mob_type not_gate = Mob_type::not_right;
        constexpr static const Mob_type or_gate = Mob_type::or_right;
        constexpr static const Mob_type nor_gate = Mob_type::nor_right;
        constexpr static const Mob_type and_gate = Mob_type::and_right;
        constexpr static const Mob_type nand_gate = Mob_type::nand_right;
        constexpr static const Mob_type xor_gate = Mob_type::xor_right;
        constexpr static const Mob_type xnor_gate = Mob_type::xnor_right;
        constexpr static bool is_component(Mob_type type)
        {
                return type == Mob_type::button || type == Mob_type::plug ||
                        type == buffer || type == not_gate ||
                        type == or_gate || type == nor_gate || type == and_gate ||
                        type == nand_gate || type == xor_gate || type == xnor_gate;
        }
};

template<> struct move_traits<Mob_move::up> {
        constexpr static const cells16v direction{0, 1, 0};
        constexpr static const char* name = "up";
        constexpr static const Mob_move reverse = Mob_move::down;
        constexpr static const Mob_type shadow = Mob_type::shadow_up;
        constexpr static const Mob_type engine = Mob_type::engine_up;
        constexpr static const Mob_type buffer = Mob_type::buffer_up;
        constexpr static const Mob_type not_gate = Mob_type::not_up;
        constexpr static const Mob_type or_gate = Mob_type::or_up;
        constexpr static const Mob_type nor_gate = Mob_type::nor_up;
        constexpr static const Mob_type and_gate = Mob_type::and_up;
        constexpr static const Mob_type nand_gate = Mob_type::nand_up;
        constexpr static const Mob_type xor_gate = Mob_type::xor_up;
        constexpr static const Mob_type xnor_gate = Mob_type::xnor_up;
        constexpr static bool is_component(Mob_type type)
        {
                return type == Mob_type::button || type == Mob_type::plug ||
                        type == buffer || type == not_gate ||
                        type == or_gate || type == nor_gate || type == and_gate ||
                        type == nand_gate || type == xor_gate || type == xnor_gate;
        }
};

template<> struct move_traits<Mob_move::down> {
        constexpr static const cells16v direction{0, -1, 0};
        constexpr static const char* name = "down";
        constexpr static const Mob_move reverse = Mob_move::up;
        constexpr static const Mob_type shadow = Mob_type::shadow_down;
        constexpr static const Mob_type engine = Mob_type::engine_down;
        constexpr static const Mob_type buffer = Mob_type::buffer_down;
        constexpr static const Mob_type not_gate = Mob_type::not_down;
        constexpr static const Mob_type or_gate = Mob_type::or_down;
        constexpr static const Mob_type nor_gate = Mob_type::nor_down;
        constexpr static const Mob_type and_gate = Mob_type::and_down;
        constexpr static const Mob_type nand_gate = Mob_type::nand_down;
        constexpr static const Mob_type xor_gate = Mob_type::xor_down;
        constexpr static const Mob_type xnor_gate = Mob_type::xnor_down;
        constexpr static bool is_component(Mob_type type)
        {
                return type == Mob_type::button || type == Mob_type::plug ||
                        type == buffer || type == not_gate ||
                        type == or_gate || type == nor_gate || type == and_gate ||
                        type == nand_gate || type == xor_gate || type == xnor_gate;
        }
};

template<> struct move_traits<Mob_move::front> {
        constexpr static const cells16v direction{0, 0, -1};
        constexpr static const char* name = "front";
        constexpr static const Mob_move reverse = Mob_move::back;
        constexpr static const Mob_type shadow = Mob_type::shadow_front;
        constexpr static const Mob_type engine = Mob_type::engine_front;
        constexpr static const Mob_type buffer = Mob_type::buffer_front;
        constexpr static const Mob_type not_gate = Mob_type::not_front;
        constexpr static const Mob_type or_gate = Mob_type::or_front;
        constexpr static const Mob_type nor_gate = Mob_type::nor_front;
        constexpr static const Mob_type and_gate = Mob_type::and_front;
        constexpr static const Mob_type nand_gate = Mob_type::nand_front;
        constexpr static const Mob_type xor_gate = Mob_type::xor_front;
        constexpr static const Mob_type xnor_gate = Mob_type::xnor_front;
        constexpr static bool is_component(Mob_type type)
        {
                return type == Mob_type::button || type == Mob_type::plug ||
                        type == buffer || type == not_gate ||
                        type == or_gate || type == nor_gate || type == and_gate ||
                        type == nand_gate || type == xor_gate || type == xnor_gate;
        }
};

template<> struct move_traits<Mob_move::back> {
        constexpr static const cells16v direction{0, 0, 1};
        constexpr static const char* name = "back";
        constexpr static const Mob_move reverse = Mob_move::front;
        constexpr static const Mob_type shadow = Mob_type::shadow_back;
        constexpr static const Mob_type engine = Mob_type::engine_back;
        constexpr static const Mob_type buffer = Mob_type::buffer_back;
        constexpr static const Mob_type not_gate = Mob_type::not_back;
        constexpr static const Mob_type or_gate = Mob_type::or_back;
        constexpr static const Mob_type nor_gate = Mob_type::nor_back;
        constexpr static const Mob_type and_gate = Mob_type::and_back;
        constexpr static const Mob_type nand_gate = Mob_type::nand_back;
        constexpr static const Mob_type xor_gate = Mob_type::xor_back;
        constexpr static const Mob_type xnor_gate = Mob_type::xnor_back;
        constexpr static bool is_component(Mob_type type)
        {
                return type == Mob_type::button || type == Mob_type::plug ||
                        type == buffer || type == not_gate ||
                        type == or_gate || type == nor_gate || type == and_gate ||
                        type == nand_gate || type == xor_gate || type == xnor_gate;
        }
};

// Return mob type from its name and vice versa

struct Mob_type_name {
        static constexpr const char* names[] = {
                "none", "fixed", "player", "crate", "lift", "hover", "button", "wire", "checkpoint",
                "goal", "plug",
                "buffer_left", "buffer_right", "buffer_up", "buffer_down", "buffer_front", "buffer_back",
                "not_left", "not_right", "not_up", "not_down", "not_front", "not_back",
                "or_left", "or_right", "or_up", "or_down", "or_front", "or_back",
                "nor_left", "nor_right", "nor_up", "nor_down", "nor_front", "nor_back",
                "and_left", "and_right", "and_up", "and_down", "and_front", "and_back",
                "nand_left", "nand_right", "nand_up", "nand_down", "nand_front", "nand_back",
                "xor_left", "xor_right", "xor_up", "xor_down", "xor_front", "xor_back",
                "xnor_left", "xnor_right", "xnor_up", "xnor_down", "xnor_front", "xnor_back",
                "engine_left", "engine_right", "engine_up", "engine_down", "engine_front", "engine_back",
                "shadow_left", "shadow_right", "shadow_up", "shadow_down", "shadow_front", "shadow_back",
                "cursor", "cursor_2",
        };

        constexpr std::string_view operator()(Mob_type type) const
        {
                return names[static_cast<size_t>(type)];
        }

        Mob_type operator()(std::string_view name) const
        {
                auto it = std::ranges::find(names, name);
                return static_cast<Mob_type>(it - std::begin(names));
        }
};

inline constexpr Mob_type_name mob_type_name;

struct Mob {
        cells16v pos;           // position of the mob in the world
        Mob_type type;          // type of the mob      
        Mob_move move;          // how the mob is moving
        int8_t frac{0};         // fraction of cell that remains to be moved
        int8_t speed{0};        // how much frac varies by frame
        bool alive : 1;         // if true, object can't be dragged
        bool falling : 1;       // if true, object is in free fall        
        bool flipped : 1;       // if true, object is rotated in the y axis
};

struct Cell {
        union {
                Mob_type type = Mob_type::none;  // type of static block
                int8_t index;   // negated index of a mob
        };
        Gravity gravity : 2 {}; // can be downwards, upwards, or none
        Power powered : 2 {};   // can be on, off, or not part of a circuit
        bool marked : 1 {};     // used by circuit algorithms
};

template<> struct std::hash<cells16v> {
        size_t operator()(const cells16v& p) const
        { return geom::get(p.x) ^ geom::get(p.y) ^ geom::get(p.z); }
};

using Cell_table = std::unordered_map<cells16v, Cell>;

struct Context {
        cells16b box;
        Cell_table table;
        std::vector<Mob> mobs;
        int8_t player = -1;
        std::bitset<32> sounds;
        std::filesystem::path music;
        size_t tick = 0;
        std::string status;
        bool update_static = true;
        bool update_mutable = true;
};

Context load_context(const std::filesystem::path& path);

void save_context(const Context& context, const std::filesystem::path& path);

#endif // SRC_CONTEXT_H_
