// nearest.h - calculate cell under the mouse

// Copyright (C) 2021 Luis Sanz <luis.sanz@gmail.com>

// This file is part of Wodox 2021.

// Wodox 2021 is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Wodox 2021 is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with Wodox 2021.  If not, see <https://www.gnu.org/licenses/>.


#ifndef SRC_NEAREST_H_
#define SRC_NEAREST_H_

#include <algorithm>

// Find the nearest non-empty cell that intersects a line defined from its
// starting point and direction. The returns value includes:
//
// - The position of the cell
// - The normal of the face that the line intersected
// - The distance to the original position, in multiples of the direction

struct Nearest_result {
        bool found;
        cells16v position;
        cells16v normal{0, 0, 0};
        float distance = std::numeric_limits<float>::max();
};

bool operator<(const Nearest_result& l, const Nearest_result& r)
{ return l.distance < r.distance; }

template<size_t Axis>
struct Nearest_cell {
 public:
        using index_type = cells16v;
        using table_type = Cell_table;
        using box_type = cells16b;
        using vec_type = glm::vec3;

 private:
        const table_type& table;
        const box_type& box;
        vec_type pos;
        const vec_type& dir;
        Nearest_result result;

        bool test(int x)
        {
                result.distance = (x - pos[Axis]) / dir[Axis];
                if constexpr (Axis == 0) result.position.x = cells16(x);
                else result.position.x = cells16((int)floor(pos.x + dir.x * result.distance));
                if constexpr (Axis == 1) result.position.y = cells16(x);
                else result.position.y = cells16((int)floor(pos.y + dir.y * result.distance));
                if constexpr (Axis == 2) result.position.z = cells16(x);
                else result.position.z = cells16((int)floor(pos.z + dir.z * result.distance));
                auto it = table.find(result.position);
                result.found = (it != table.end() && it->second.type != Mob_type::none);
                return result.found;
        }

 public:
        Nearest_cell(const table_type& table, const box_type& box,
                        const vec_type& pos, const vec_type& dir)
                : table{table}, box{box}, pos{pos}, dir{dir} {}

        Nearest_result operator()()
        {
                if (dir[Axis] > 0) {
                        int x = std::clamp<int>(floor(pos[Axis]),
                                        (int)box[Axis].min,
                                        (int)box[Axis].max - 1);
                        while (!test(x) && x < (int)box[Axis].max) ++x;
                        result.normal[Axis] = -1_cl;
                } else if (dir[Axis] < 0) {
                        int x = std::clamp<int>(floor(pos[Axis]),
                                        (int)box[Axis].min,
                                        (int)box[Axis].max - 1);
                        --pos[Axis];
                        while (!test(x) && x >= (int)box[Axis].min) --x;
                        result.normal[Axis] = 1_cl;
                }
                return result;
        }
};

Nearest_result nearest_cell(const Cell_table& table, const cells16b& box,
                const glm::vec3& pos, const glm::vec3& dir)
{
        return std::min({
                Nearest_cell<0>{table, box, pos, dir}(),
                Nearest_cell<1>{table, box, pos, dir}(),
                Nearest_cell<2>{table, box, pos, dir}(),
                });
}

#endif // SRC_NEAREST_H_
