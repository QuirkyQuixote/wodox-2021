// config.cc - load and save configuration

// Copyright (C) 2021 Luis Sanz <luis.sanz@gmail.com>

// This file is part of Wodox 2021.

// Wodox 2021 is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Wodox 2021 is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with Wodox 2021.  If not, see <https://www.gnu.org/licenses/>.


#include "config.h"

#include <fstream>

#include "media.h"
#include "mdea/mdeaxx.h"
#include "xdgbds.h"

SDL_AudioFormat get_audio_format_from_name(const std::string_view& name)
{
        if (name == "s8") return AUDIO_S8;
        if (name == "u8") return AUDIO_U8;
        if (name == "s16lsb") return AUDIO_S16LSB;
        if (name == "s16msb") return AUDIO_S16MSB;
        if (name == "s16sys") return AUDIO_S16SYS;
        if (name == "s16") return AUDIO_S16;
        if (name == "u16lsb") return AUDIO_U16LSB;
        if (name == "u16msb") return AUDIO_U16MSB;
        if (name == "u16sys") return AUDIO_U16SYS;
        if (name == "u16") return AUDIO_U16;
        if (name == "u32lsb") return AUDIO_S32LSB;
        if (name == "s32msb") return AUDIO_S32MSB;
        if (name == "s32sys") return AUDIO_S32SYS;
        if (name == "s32") return AUDIO_S32;
        if (name == "f32lsb") return AUDIO_F32LSB;
        if (name == "f32msb") return AUDIO_F32MSB;
        if (name == "f32sys") return AUDIO_F32SYS;
        if (name == "f32") return AUDIO_F32;
        throw std::runtime_error{"Bad audio format: " + std::string(name)};
}

std::string_view get_audio_format_name(SDL_AudioFormat n)
{
        if (n == AUDIO_S8) return "s8";
        if (n == AUDIO_U8) return "u8";
        if (n == AUDIO_S16LSB) return "s16lsb";
        if (n == AUDIO_S16MSB) return "s16msb";
        if (n == AUDIO_S16SYS) return "s16sys";
        if (n == AUDIO_S16) return "s16";
        if (n == AUDIO_U16LSB) return "u16lsb";
        if (n == AUDIO_U16MSB) return "u16msb";
        if (n == AUDIO_U16SYS) return "u16sys";
        if (n == AUDIO_U16) return "u16";
        if (n == AUDIO_S32LSB) return "u32lsb";
        if (n == AUDIO_S32MSB) return "s32msb";
        if (n == AUDIO_S32SYS) return "s32sys";
        if (n == AUDIO_S32) return "s32";
        if (n == AUDIO_F32LSB) return "f32lsb";
        if (n == AUDIO_F32MSB) return "f32msb";
        if (n == AUDIO_F32SYS) return "f32sys";
        if (n == AUDIO_F32) return "f32";
        throw std::runtime_error{"Bad audio format: " + std::to_string(n)};
}

struct Config_loader {
        Config config;

        Config operator()(const std::filesystem::path& path)
        {
                if (auto doc = mdea::document{path}) {
                        parse(doc.root().get_object());
                        return std::move(config);
                }
                throw std::runtime_error{path.string() + ": bad JSON"};
        }

        void parse(const mdea::object& obj)
        {
                if (auto x = obj["window"])
                        parse_window(x.get_object());
                if (auto x = obj["audio"])
                        parse_window(x.get_object());
                if (auto x = obj["game"])
                        parse_window(x.get_object());
                if (auto x = obj["edit"])
                        parse_window(x.get_object());
        }

        void parse_window(const mdea::object& obj)
        {
                if (auto x = obj["width"])
                        config.window.width = x.get_long();
                if (auto x = obj["height"])
                        config.window.height = x.get_long();
                if (auto x = obj["fullscreen"])
                        config.window.fullscreen = x.get_long();
                if (auto x = obj["vsync"])
                        config.window.width = x.get_long();
        }

        void parse_audio(const mdea::object& obj)
        {
                if (auto x = obj["frequency"])
                        config.audio.frequency = x.get_long();
                if (auto x = obj["format"])
                        config.audio.format = parse_audio_format(x.get_string());
                if (auto x = obj["channels"])
                        config.audio.channels = x.get_long();
                if (auto x = obj["chunksize"])
                        config.audio.chunksize = x.get_long();
                if (auto x = obj["music_volume"])
                        config.audio.music_volume = x.get_long();
                if (auto x = obj["sfx_volume"])
                        config.audio.sfx_volume = x.get_long();
        }

        void parse_game(const mdea::object& obj)
        {
                if (auto x = obj["left"])
                        config.game.left = parse_inputs(x.get_array());
                if (auto x = obj["right"])
                        config.game.right = parse_inputs(x.get_array());
                if (auto x = obj["up"])
                        config.game.up = parse_inputs(x.get_array());
                if (auto x = obj["down"])
                        config.game.down = parse_inputs(x.get_array());
                if (auto x = obj["cw"])
                        config.game.cw = parse_inputs(x.get_array());
                if (auto x = obj["ccw"])
                        config.game.ccw = parse_inputs(x.get_array());
        }

        void parse_edit(const mdea::object& obj)
        {
                if (auto x = obj["left"])
                        config.edit.left = parse_inputs(x.get_array());
                if (auto x = obj["right"])
                        config.edit.right = parse_inputs(x.get_array());
                if (auto x = obj["up"])
                        config.edit.up = parse_inputs(x.get_array());
                if (auto x = obj["down"])
                        config.edit.down = parse_inputs(x.get_array());
                if (auto x = obj["front"])
                        config.edit.front = parse_inputs(x.get_array());
                if (auto x = obj["back"])
                        config.edit.back = parse_inputs(x.get_array());
                if (auto x = obj["erase"])
                        config.edit.erase = parse_inputs(x.get_array());
                if (auto x = obj["paint"])
                        config.edit.paint = parse_inputs(x.get_array());
        }

        SDL_AudioFormat parse_audio_format(const std::string_view& str)
        { return get_audio_format_from_name(str); }

        std::vector<Event_catcher> parse_inputs(const mdea::array& arr)
        {
                std::vector<Event_catcher> catchers;
                for (auto x : arr)
                        catchers.push_back(parse_input(x.get_string()));
                return catchers;
        }

        Event_catcher parse_input(const std::string_view& str)
        {
                Event_catcher catcher;
                std::string buf{str};
                if (std::istringstream{buf} >> catcher)
                        return catcher;
                throw std::runtime_error{"bad input: " + buf};
        }
};

Config load_config(const std::filesystem::path& path)
{
        return Config_loader{}(path);
}

std::ostream& operator<<(std::ostream& stream, const std::vector<Event_catcher>& catchers)
{
        stream << "[";
        bool first = true;
        for (auto& catcher : catchers) {
                if (first) first = false;
                else stream << ",";
                stream << "\"" << catcher << "\"";
        }
        return stream << "]";
}

std::ostream& operator<<(std::ostream& stream, const Config& config)
{
        stream << "{\n";
        stream << "        \"window\":{\n";
        stream << "                \"width\":" << config.window.width << ",\n";
        stream << "                \"height\":" << config.window.height << ",\n";
        stream << "                \"fullscreen\":" << config.window.fullscreen << ",\n";
        stream << "                \"vsync\":" << config.window.vsync << "\n";
        stream << "        },\n";
        stream << "        \"audio\":{\n";
        stream << "                \"frequency\":" << config.audio.frequency << ",\n";
        stream << "                \"format\":\"" << get_audio_format_name(config.audio.format) << "\",\n";
        stream << "                \"channels\":" << config.audio.channels << ",\n";
        stream << "                \"chunksize\":" << config.audio.chunksize << ",\n";
        stream << "                \"music_volume\":" << config.audio.music_volume << ",\n";
        stream << "                \"sfx_volume\":" << config.audio.sfx_volume << "\n";
        stream << "        },\n";
        stream << "        \"game\":{\n";
        stream << "                \"left\":" << config.game.left << ",\n";
        stream << "                \"right\":" << config.game.right << ",\n";
        stream << "                \"up\":" << config.game.up << ",\n";
        stream << "                \"down\":" << config.game.down << ",\n";
        stream << "                \"ccw\":" << config.game.ccw << ",\n";
        stream << "                \"cw\":" << config.game.cw << "\n";
        stream << "        },\n";
        stream << "        \"edit\":{\n";
        stream << "                \"left\":" << config.edit.left << ",\n";
        stream << "                \"right\":" << config.edit.right << ",\n";
        stream << "                \"up\":" << config.edit.up << ",\n";
        stream << "                \"down\":" << config.edit.down << ",\n";
        stream << "                \"front\":" << config.edit.front << ",\n";
        stream << "                \"back\":" << config.edit.back << ",\n";
        stream << "                \"erase\":" << config.edit.erase << ",\n";
        stream << "                \"paint\":" << config.edit.paint << "\n";
        stream << "        }\n";
        stream << "}\n";
        return stream;
}

void save_config(const Config& config, const std::filesystem::path& path)
{
        std::ofstream stream{path.string()};
        if (stream) stream << config;
        else throw std::runtime_error{"Can't open " + path.string()};
}

Config load_config()
{
        Config config;
        try {
                config = load_config(xdg::config::find(BASEDIR ".json"));
        } catch (std::exception& ex) {
                std::cerr << ex.what() << "\n";
                std::cerr << "Defaulting to hardcoded configuration.\n";
        }
        return config;
}

void save_config(const Config& config)
{
        auto path = xdg::config::home() / BASEDIR ".json";
        save_config(config, path);
        std::cout << "Config file saved to " << path.string() << "\n\n";
}

