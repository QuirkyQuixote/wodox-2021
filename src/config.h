// config.h - declare Config type and related functions

// Copyright (C) 2021 Luis Sanz <luis.sanz@gmail.com>

// This file is part of Wodox 2021.

// Wodox 2021 is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Wodox 2021 is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with Wodox 2021.  If not, see <https://www.gnu.org/licenses/>.


#ifndef SRC_CONFIG_H_
#define SRC_CONFIG_H_

#include <filesystem>

#include "media.h"
#include "event.h"

// Configuration 

struct Audio_config {
        int frequency = MIX_DEFAULT_FREQUENCY;
        Uint16 format = MIX_DEFAULT_FORMAT;
        int channels = 2;
        int chunksize = 1024;
        int music_volume = MIX_MAX_VOLUME;
        int sfx_volume = MIX_MAX_VOLUME;
};

struct Game_input_config {
        std::vector<Event_catcher> left{
                Controller_button_catcher{0, SDL_CONTROLLER_BUTTON_DPAD_LEFT},
                Scancode_catcher{SDL_SCANCODE_LEFT},
                Scancode_catcher{SDL_SCANCODE_A},
                Scancode_catcher{SDL_SCANCODE_J}
        };
        std::vector<Event_catcher> right{
                Controller_button_catcher{0, SDL_CONTROLLER_BUTTON_DPAD_RIGHT},
                Scancode_catcher{SDL_SCANCODE_RIGHT},
                Scancode_catcher{SDL_SCANCODE_D},
                Scancode_catcher{SDL_SCANCODE_L}
        };
        std::vector<Event_catcher> up{
                Controller_button_catcher{0, SDL_CONTROLLER_BUTTON_DPAD_UP},
                Scancode_catcher{SDL_SCANCODE_UP},
                Scancode_catcher{SDL_SCANCODE_W},
                Scancode_catcher{SDL_SCANCODE_I}
        };
        std::vector<Event_catcher> down{
                Controller_button_catcher{0, SDL_CONTROLLER_BUTTON_DPAD_DOWN},
                Scancode_catcher{SDL_SCANCODE_DOWN},
                Scancode_catcher{SDL_SCANCODE_S},
                Scancode_catcher{SDL_SCANCODE_K}
        };
        std::vector<Event_catcher> ccw{
                Controller_button_catcher{0, SDL_CONTROLLER_BUTTON_Y},
                Scancode_catcher{SDL_SCANCODE_Z},
                Scancode_catcher{SDL_SCANCODE_Q},
                Scancode_catcher{SDL_SCANCODE_U}
        };
        std::vector<Event_catcher> cw{
                Controller_button_catcher{0, SDL_CONTROLLER_BUTTON_B},
                Scancode_catcher{SDL_SCANCODE_X},
                Scancode_catcher{SDL_SCANCODE_E},
                Scancode_catcher{SDL_SCANCODE_O}
        };
};

struct Edit_input_config {
        std::vector<Event_catcher> left{Scancode_catcher{SDL_SCANCODE_A}};
        std::vector<Event_catcher> right{Scancode_catcher{SDL_SCANCODE_D}};
        std::vector<Event_catcher> up{Scancode_catcher{SDL_SCANCODE_SPACE}};
        std::vector<Event_catcher> down{Scancode_catcher{SDL_SCANCODE_LCTRL}};
        std::vector<Event_catcher> front{Scancode_catcher{SDL_SCANCODE_W}};
        std::vector<Event_catcher> back{Scancode_catcher{SDL_SCANCODE_S}};
        std::vector<Event_catcher> paint{Mouse_button_catcher{0, SDL_BUTTON_RIGHT}};
        std::vector<Event_catcher> erase{Mouse_button_catcher{0, SDL_BUTTON_LEFT}};
        std::vector<Event_catcher> pick{Mouse_button_catcher{0, SDL_BUTTON_MIDDLE}};
};

struct Config {
        Window_config window;
        Audio_config audio;
        Game_input_config game;
        Edit_input_config edit;
};

Config load_config(const std::filesystem::path& path);
void save_config(const Config& config, const std::filesystem::path& path);

Config load_config();
void save_config(const Config& config);

#endif // SRC_CONFIG_H_
