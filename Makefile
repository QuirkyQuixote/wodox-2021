
# Where this file resides

root_dir := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))

# When version changes, increment this number

VERSION := 1.0.0

# Determine where we are going to install things

prefix := $(HOME)/.local
bindir := $(prefix)/bin
datadir := $(prefix)/share/wodox2021

# According to the GNU Make documentation: "Every Makefile should define the
# variable INSTALL, which is the basic command for installing a file into the
# system.  Every Makefile should also define the variables INSTALL_PROGRAM and
# INSTALL_DATA."

INSTALL := install
INSTALL_PROGRAM := $(INSTALL)
INSTALL_DATA := $(INSTALL) -m 644

# These flags may be overriden by the user

CXXFLAGS ?= -g
CPPFLAGS ?= $(shell pkg-config --cflags sdl2 SDL2_image SDL2_mixer glew gl)
LDLIBS ?= $(shell pkg-config --libs sdl2 SDL2_image SDL2_mixer glew gl)

# These flags are necessary

override CPPFLAGS += -DVERSION=\"$(VERSION)\"
override CPPFLAGS += -DBASEDIR=\"wodox2021\"
override CPPFLAGS += -I$(root_dir)geom
override CPPFLAGS += -I$(root_dir)mdea
override CPPFLAGS += -I$(root_dir)xdgbds

override CXXFLAGS += -std=c++20
override CXXFLAGS += -fPIC
override CXXFLAGS += -MMD
override CXXFLAGS += -Wall
override CXXFLAGS += -Werror
override CXXFLAGS += -Wfatal-errors

# Compilation takes place in...

vpath %.cc $(root_dir)src

# And now, the rules.

.PHONY: all
all: submodules edit play

.PHONY: media
media: png ogg

.PHONY: clean
clean:
	$(RM) *.o
	$(RM) *.d
	$(RM) edit
	$(RM) play

.PHONY: install
install: all
	$(INSTALL) -d $(DESTDIR)$(bindir)
	$(INSTALL_PROGRAM) play $(DESTDIR)$(bindir)/wodox2021-play
	$(INSTALL_PROGRAM) edit $(DESTDIR)$(bindir)/wodox2021-edit
	$(INSTALL) -d $(DESTDIR)$(datadir)
	$(INSTALL) -d $(DESTDIR)$(datadir)/textures
	$(INSTALL_DATA) $(root_dir)data/textures/textures.png $(DESTDIR)$(datadir)/textures
	$(INSTALL_DATA) $(root_dir)data/textures/font.png $(DESTDIR)$(datadir)/textures
	$(INSTALL_DATA) $(root_dir)data/textures/skybox_1.png $(DESTDIR)$(datadir)/textures
	$(INSTALL_DATA) $(root_dir)data/textures/skybox_2.png $(DESTDIR)$(datadir)/textures
	$(INSTALL_DATA) $(root_dir)data/textures/skybox_3.png $(DESTDIR)$(datadir)/textures
	$(INSTALL_DATA) $(root_dir)data/textures/skybox_4.png $(DESTDIR)$(datadir)/textures
	$(INSTALL_DATA) $(root_dir)data/textures/skybox_5.png $(DESTDIR)$(datadir)/textures
	$(INSTALL_DATA) $(root_dir)data/textures/skybox_6.png $(DESTDIR)$(datadir)/textures
	$(INSTALL) -d $(DESTDIR)$(datadir)/shaders
	$(INSTALL_DATA) $(root_dir)data/shaders/geometry.vert $(DESTDIR)$(datadir)/shaders
	$(INSTALL_DATA) $(root_dir)data/shaders/geometry.frag $(DESTDIR)$(datadir)/shaders
	$(INSTALL_DATA) $(root_dir)data/shaders/gauss.vert $(DESTDIR)$(datadir)/shaders
	$(INSTALL_DATA) $(root_dir)data/shaders/gauss.frag $(DESTDIR)$(datadir)/shaders
	$(INSTALL_DATA) $(root_dir)data/shaders/final.vert $(DESTDIR)$(datadir)/shaders
	$(INSTALL_DATA) $(root_dir)data/shaders/final.frag $(DESTDIR)$(datadir)/shaders
	$(INSTALL_DATA) $(root_dir)data/shaders/text.vert $(DESTDIR)$(datadir)/shaders
	$(INSTALL_DATA) $(root_dir)data/shaders/text.frag $(DESTDIR)$(datadir)/shaders
	$(INSTALL_DATA) $(root_dir)data/shaders/skybox.vert $(DESTDIR)$(datadir)/shaders
	$(INSTALL_DATA) $(root_dir)data/shaders/skybox.frag $(DESTDIR)$(datadir)/shaders
	$(INSTALL) -d $(DESTDIR)$(datadir)/sounds
	$(INSTALL_DATA) $(root_dir)data/sounds/player.flac $(DESTDIR)$(datadir)/sounds
	$(INSTALL_DATA) $(root_dir)data/sounds/press.flac $(DESTDIR)$(datadir)/sounds
	$(INSTALL_DATA) $(root_dir)data/sounds/release.flac $(DESTDIR)$(datadir)/sounds
	$(INSTALL_DATA) $(root_dir)data/sounds/land.flac $(DESTDIR)$(datadir)/sounds
	$(INSTALL_DATA) $(root_dir)data/sounds/camera.flac $(DESTDIR)$(datadir)/sounds
	$(INSTALL_DATA) $(root_dir)data/sounds/write.flac $(DESTDIR)$(datadir)/sounds
	$(INSTALL_DATA) $(root_dir)data/sounds/flip.flac $(DESTDIR)$(datadir)/sounds
	$(INSTALL) -d $(DESTDIR)$(datadir)/music
	$(INSTALL_DATA) $(root_dir)data/music/track0.ogg $(DESTDIR)$(datadir)/music
	$(INSTALL_DATA) $(root_dir)data/music/track1.ogg $(DESTDIR)$(datadir)/music
	$(INSTALL_DATA) $(root_dir)data/music/track2.ogg $(DESTDIR)$(datadir)/music
	$(INSTALL_DATA) $(root_dir)data/music/track3.ogg $(DESTDIR)$(datadir)/music
	$(INSTALL_DATA) $(root_dir)data/music/track4.ogg $(DESTDIR)$(datadir)/music
	$(INSTALL_DATA) $(root_dir)data/music/track5.ogg $(DESTDIR)$(datadir)/music
	$(INSTALL_DATA) $(root_dir)data/music/track6.ogg $(DESTDIR)$(datadir)/music
	$(INSTALL_DATA) $(root_dir)data/music/track7.ogg $(DESTDIR)$(datadir)/music
	$(INSTALL) -d $(DESTDIR)$(datadir)/models
	$(INSTALL_DATA) $(root_dir)data/models/and_back_off.json $(DESTDIR)$(datadir)/models
	$(INSTALL_DATA) $(root_dir)data/models/and_back_on.json $(DESTDIR)$(datadir)/models
	$(INSTALL_DATA) $(root_dir)data/models/and_down_off.json $(DESTDIR)$(datadir)/models
	$(INSTALL_DATA) $(root_dir)data/models/and_down_on.json $(DESTDIR)$(datadir)/models
	$(INSTALL_DATA) $(root_dir)data/models/and_front_off.json $(DESTDIR)$(datadir)/models
	$(INSTALL_DATA) $(root_dir)data/models/and_front_on.json $(DESTDIR)$(datadir)/models
	$(INSTALL_DATA) $(root_dir)data/models/and_left_off.json $(DESTDIR)$(datadir)/models
	$(INSTALL_DATA) $(root_dir)data/models/and_left_on.json $(DESTDIR)$(datadir)/models
	$(INSTALL_DATA) $(root_dir)data/models/and_right_off.json $(DESTDIR)$(datadir)/models
	$(INSTALL_DATA) $(root_dir)data/models/and_right_on.json $(DESTDIR)$(datadir)/models
	$(INSTALL_DATA) $(root_dir)data/models/and_up_off.json $(DESTDIR)$(datadir)/models
	$(INSTALL_DATA) $(root_dir)data/models/and_up_on.json $(DESTDIR)$(datadir)/models
	$(INSTALL_DATA) $(root_dir)data/models/buffer_back_off.json $(DESTDIR)$(datadir)/models
	$(INSTALL_DATA) $(root_dir)data/models/buffer_back_on.json $(DESTDIR)$(datadir)/models
	$(INSTALL_DATA) $(root_dir)data/models/buffer_down_off.json $(DESTDIR)$(datadir)/models
	$(INSTALL_DATA) $(root_dir)data/models/buffer_down_on.json $(DESTDIR)$(datadir)/models
	$(INSTALL_DATA) $(root_dir)data/models/buffer_front_off.json $(DESTDIR)$(datadir)/models
	$(INSTALL_DATA) $(root_dir)data/models/buffer_front_on.json $(DESTDIR)$(datadir)/models
	$(INSTALL_DATA) $(root_dir)data/models/buffer_left_off.json $(DESTDIR)$(datadir)/models
	$(INSTALL_DATA) $(root_dir)data/models/buffer_left_on.json $(DESTDIR)$(datadir)/models
	$(INSTALL_DATA) $(root_dir)data/models/buffer_right_off.json $(DESTDIR)$(datadir)/models
	$(INSTALL_DATA) $(root_dir)data/models/buffer_right_on.json $(DESTDIR)$(datadir)/models
	$(INSTALL_DATA) $(root_dir)data/models/buffer_up_off.json $(DESTDIR)$(datadir)/models
	$(INSTALL_DATA) $(root_dir)data/models/buffer_up_on.json $(DESTDIR)$(datadir)/models
	$(INSTALL_DATA) $(root_dir)data/models/button_off.json $(DESTDIR)$(datadir)/models
	$(INSTALL_DATA) $(root_dir)data/models/button_on.json $(DESTDIR)$(datadir)/models
	$(INSTALL_DATA) $(root_dir)data/models/checkpoint.json $(DESTDIR)$(datadir)/models
	$(INSTALL_DATA) $(root_dir)data/models/crate.json $(DESTDIR)$(datadir)/models
	$(INSTALL_DATA) $(root_dir)data/models/cursor.json $(DESTDIR)$(datadir)/models
	$(INSTALL_DATA) $(root_dir)data/models/engine.json $(DESTDIR)$(datadir)/models
	$(INSTALL_DATA) $(root_dir)data/models/fixed.json $(DESTDIR)$(datadir)/models
	$(INSTALL_DATA) $(root_dir)data/models/fixed2.json $(DESTDIR)$(datadir)/models
	$(INSTALL_DATA) $(root_dir)data/models/fixed3.json $(DESTDIR)$(datadir)/models
	$(INSTALL_DATA) $(root_dir)data/models/fixed4.json $(DESTDIR)$(datadir)/models
	$(INSTALL_DATA) $(root_dir)data/models/fixed5.json $(DESTDIR)$(datadir)/models
	$(INSTALL_DATA) $(root_dir)data/models/goal.json $(DESTDIR)$(datadir)/models
	$(INSTALL_DATA) $(root_dir)data/models/hover.json $(DESTDIR)$(datadir)/models
	$(INSTALL_DATA) $(root_dir)data/models/index.json $(DESTDIR)$(datadir)/models
	$(INSTALL_DATA) $(root_dir)data/models/lift.json $(DESTDIR)$(datadir)/models
	$(INSTALL_DATA) $(root_dir)data/models/nand_back_off.json $(DESTDIR)$(datadir)/models
	$(INSTALL_DATA) $(root_dir)data/models/nand_back_on.json $(DESTDIR)$(datadir)/models
	$(INSTALL_DATA) $(root_dir)data/models/nand_down_off.json $(DESTDIR)$(datadir)/models
	$(INSTALL_DATA) $(root_dir)data/models/nand_down_on.json $(DESTDIR)$(datadir)/models
	$(INSTALL_DATA) $(root_dir)data/models/nand_front_off.json $(DESTDIR)$(datadir)/models
	$(INSTALL_DATA) $(root_dir)data/models/nand_front_on.json $(DESTDIR)$(datadir)/models
	$(INSTALL_DATA) $(root_dir)data/models/nand_left_off.json $(DESTDIR)$(datadir)/models
	$(INSTALL_DATA) $(root_dir)data/models/nand_left_on.json $(DESTDIR)$(datadir)/models
	$(INSTALL_DATA) $(root_dir)data/models/nand_right_off.json $(DESTDIR)$(datadir)/models
	$(INSTALL_DATA) $(root_dir)data/models/nand_right_on.json $(DESTDIR)$(datadir)/models
	$(INSTALL_DATA) $(root_dir)data/models/nand_up_off.json $(DESTDIR)$(datadir)/models
	$(INSTALL_DATA) $(root_dir)data/models/nand_up_on.json $(DESTDIR)$(datadir)/models
	$(INSTALL_DATA) $(root_dir)data/models/nor_back_off.json $(DESTDIR)$(datadir)/models
	$(INSTALL_DATA) $(root_dir)data/models/nor_back_on.json $(DESTDIR)$(datadir)/models
	$(INSTALL_DATA) $(root_dir)data/models/nor_down_off.json $(DESTDIR)$(datadir)/models
	$(INSTALL_DATA) $(root_dir)data/models/nor_down_on.json $(DESTDIR)$(datadir)/models
	$(INSTALL_DATA) $(root_dir)data/models/nor_front_off.json $(DESTDIR)$(datadir)/models
	$(INSTALL_DATA) $(root_dir)data/models/nor_front_on.json $(DESTDIR)$(datadir)/models
	$(INSTALL_DATA) $(root_dir)data/models/nor_left_off.json $(DESTDIR)$(datadir)/models
	$(INSTALL_DATA) $(root_dir)data/models/nor_left_on.json $(DESTDIR)$(datadir)/models
	$(INSTALL_DATA) $(root_dir)data/models/nor_right_off.json $(DESTDIR)$(datadir)/models
	$(INSTALL_DATA) $(root_dir)data/models/nor_right_on.json $(DESTDIR)$(datadir)/models
	$(INSTALL_DATA) $(root_dir)data/models/nor_up_off.json $(DESTDIR)$(datadir)/models
	$(INSTALL_DATA) $(root_dir)data/models/nor_up_on.json $(DESTDIR)$(datadir)/models
	$(INSTALL_DATA) $(root_dir)data/models/not_back_off.json $(DESTDIR)$(datadir)/models
	$(INSTALL_DATA) $(root_dir)data/models/not_back_on.json $(DESTDIR)$(datadir)/models
	$(INSTALL_DATA) $(root_dir)data/models/not_down_off.json $(DESTDIR)$(datadir)/models
	$(INSTALL_DATA) $(root_dir)data/models/not_down_on.json $(DESTDIR)$(datadir)/models
	$(INSTALL_DATA) $(root_dir)data/models/not_front_off.json $(DESTDIR)$(datadir)/models
	$(INSTALL_DATA) $(root_dir)data/models/not_front_on.json $(DESTDIR)$(datadir)/models
	$(INSTALL_DATA) $(root_dir)data/models/not_left_off.json $(DESTDIR)$(datadir)/models
	$(INSTALL_DATA) $(root_dir)data/models/not_left_on.json $(DESTDIR)$(datadir)/models
	$(INSTALL_DATA) $(root_dir)data/models/not_right_off.json $(DESTDIR)$(datadir)/models
	$(INSTALL_DATA) $(root_dir)data/models/not_right_on.json $(DESTDIR)$(datadir)/models
	$(INSTALL_DATA) $(root_dir)data/models/not_up_off.json $(DESTDIR)$(datadir)/models
	$(INSTALL_DATA) $(root_dir)data/models/not_up_on.json $(DESTDIR)$(datadir)/models
	$(INSTALL_DATA) $(root_dir)data/models/or_back_off.json $(DESTDIR)$(datadir)/models
	$(INSTALL_DATA) $(root_dir)data/models/or_back_on.json $(DESTDIR)$(datadir)/models
	$(INSTALL_DATA) $(root_dir)data/models/or_down_off.json $(DESTDIR)$(datadir)/models
	$(INSTALL_DATA) $(root_dir)data/models/or_down_on.json $(DESTDIR)$(datadir)/models
	$(INSTALL_DATA) $(root_dir)data/models/or_front_off.json $(DESTDIR)$(datadir)/models
	$(INSTALL_DATA) $(root_dir)data/models/or_front_on.json $(DESTDIR)$(datadir)/models
	$(INSTALL_DATA) $(root_dir)data/models/or_left_off.json $(DESTDIR)$(datadir)/models
	$(INSTALL_DATA) $(root_dir)data/models/or_left_on.json $(DESTDIR)$(datadir)/models
	$(INSTALL_DATA) $(root_dir)data/models/or_right_off.json $(DESTDIR)$(datadir)/models
	$(INSTALL_DATA) $(root_dir)data/models/or_right_on.json $(DESTDIR)$(datadir)/models
	$(INSTALL_DATA) $(root_dir)data/models/or_up_off.json $(DESTDIR)$(datadir)/models
	$(INSTALL_DATA) $(root_dir)data/models/or_up_on.json $(DESTDIR)$(datadir)/models
	$(INSTALL_DATA) $(root_dir)data/models/player_fb.json $(DESTDIR)$(datadir)/models
	$(INSTALL_DATA) $(root_dir)data/models/player_lr.json $(DESTDIR)$(datadir)/models
	$(INSTALL_DATA) $(root_dir)data/models/plug_off.json $(DESTDIR)$(datadir)/models
	$(INSTALL_DATA) $(root_dir)data/models/plug_on.json $(DESTDIR)$(datadir)/models
	$(INSTALL_DATA) $(root_dir)data/models/wire_off.json $(DESTDIR)$(datadir)/models
	$(INSTALL_DATA) $(root_dir)data/models/wire_on.json $(DESTDIR)$(datadir)/models
	$(INSTALL_DATA) $(root_dir)data/models/xnor_back_off.json $(DESTDIR)$(datadir)/models
	$(INSTALL_DATA) $(root_dir)data/models/xnor_back_on.json $(DESTDIR)$(datadir)/models
	$(INSTALL_DATA) $(root_dir)data/models/xnor_down_off.json $(DESTDIR)$(datadir)/models
	$(INSTALL_DATA) $(root_dir)data/models/xnor_down_on.json $(DESTDIR)$(datadir)/models
	$(INSTALL_DATA) $(root_dir)data/models/xnor_front_off.json $(DESTDIR)$(datadir)/models
	$(INSTALL_DATA) $(root_dir)data/models/xnor_front_on.json $(DESTDIR)$(datadir)/models
	$(INSTALL_DATA) $(root_dir)data/models/xnor_left_off.json $(DESTDIR)$(datadir)/models
	$(INSTALL_DATA) $(root_dir)data/models/xnor_left_on.json $(DESTDIR)$(datadir)/models
	$(INSTALL_DATA) $(root_dir)data/models/xnor_right_off.json $(DESTDIR)$(datadir)/models
	$(INSTALL_DATA) $(root_dir)data/models/xnor_right_on.json $(DESTDIR)$(datadir)/models
	$(INSTALL_DATA) $(root_dir)data/models/xnor_up_off.json $(DESTDIR)$(datadir)/models
	$(INSTALL_DATA) $(root_dir)data/models/xnor_up_on.json $(DESTDIR)$(datadir)/models
	$(INSTALL_DATA) $(root_dir)data/models/xor_back_off.json $(DESTDIR)$(datadir)/models
	$(INSTALL_DATA) $(root_dir)data/models/xor_back_on.json $(DESTDIR)$(datadir)/models
	$(INSTALL_DATA) $(root_dir)data/models/xor_down_off.json $(DESTDIR)$(datadir)/models
	$(INSTALL_DATA) $(root_dir)data/models/xor_down_on.json $(DESTDIR)$(datadir)/models
	$(INSTALL_DATA) $(root_dir)data/models/xor_front_off.json $(DESTDIR)$(datadir)/models
	$(INSTALL_DATA) $(root_dir)data/models/xor_front_on.json $(DESTDIR)$(datadir)/models
	$(INSTALL_DATA) $(root_dir)data/models/xor_left_off.json $(DESTDIR)$(datadir)/models
	$(INSTALL_DATA) $(root_dir)data/models/xor_left_on.json $(DESTDIR)$(datadir)/models
	$(INSTALL_DATA) $(root_dir)data/models/xor_right_off.json $(DESTDIR)$(datadir)/models
	$(INSTALL_DATA) $(root_dir)data/models/xor_right_on.json $(DESTDIR)$(datadir)/models
	$(INSTALL_DATA) $(root_dir)data/models/xor_up_off.json $(DESTDIR)$(datadir)/models
	$(INSTALL_DATA) $(root_dir)data/models/xor_up_on.json $(DESTDIR)$(datadir)/models

.PHONY: uninstall
uninstall:
	-$(RM) $(DESTDIR)$(bindir)/wodox2021-play
	-$(RM) $(DESTDIR)$(bindir)/wodox2021-edit
	-$(RM) -d $(DESTDIR)$(bindir)
	-$(RM) $(DESTDIR)$(datadir)/textures/textures.png
	-$(RM) $(DESTDIR)$(datadir)/textures/font.png
	-$(RM) $(DESTDIR)$(datadir)/textures/skybox_1.png
	-$(RM) $(DESTDIR)$(datadir)/textures/skybox_2.png
	-$(RM) $(DESTDIR)$(datadir)/textures/skybox_3.png
	-$(RM) $(DESTDIR)$(datadir)/textures/skybox_4.png
	-$(RM) $(DESTDIR)$(datadir)/textures/skybox_5.png
	-$(RM) $(DESTDIR)$(datadir)/textures/skybox_6.png
	-$(RM) -d $(DESTDIR)$(datadir)/textures
	-$(RM) $(DESTDIR)$(datadir)/shaders/geometry.vert
	-$(RM) $(DESTDIR)$(datadir)/shaders/geometry.frag
	-$(RM) $(DESTDIR)$(datadir)/shaders/gauss.vert
	-$(RM) $(DESTDIR)$(datadir)/shaders/gauss.frag
	-$(RM) $(DESTDIR)$(datadir)/shaders/final.vert
	-$(RM) $(DESTDIR)$(datadir)/shaders/final.frag
	-$(RM) $(DESTDIR)$(datadir)/shaders/text.vert
	-$(RM) $(DESTDIR)$(datadir)/shaders/text.frag
	-$(RM) $(DESTDIR)$(datadir)/shaders/skybox.vert
	-$(RM) $(DESTDIR)$(datadir)/shaders/skybox.frag
	-$(RM) -d $(DESTDIR)$(datadir)/shaders
	-$(RM) $(DESTDIR)$(datadir)/sounds/player.flac
	-$(RM) $(DESTDIR)$(datadir)/sounds/press.flac
	-$(RM) $(DESTDIR)$(datadir)/sounds/release.flac
	-$(RM) $(DESTDIR)$(datadir)/sounds/land.flac
	-$(RM) $(DESTDIR)$(datadir)/sounds/camera.flac
	-$(RM) $(DESTDIR)$(datadir)/sounds/write.flac
	-$(RM) $(DESTDIR)$(datadir)/sounds/flip.flac
	-$(RM) -d $(DESTDIR)$(datadir)/sounds
	-$(RM) $(DESTDIR)$(datadir)/music/track0.ogg
	-$(RM) $(DESTDIR)$(datadir)/music/track1.ogg
	-$(RM) $(DESTDIR)$(datadir)/music/track2.ogg
	-$(RM) $(DESTDIR)$(datadir)/music/track3.ogg
	-$(RM) $(DESTDIR)$(datadir)/music/track4.ogg
	-$(RM) $(DESTDIR)$(datadir)/music/track5.ogg
	-$(RM) $(DESTDIR)$(datadir)/music/track6.ogg
	-$(RM) $(DESTDIR)$(datadir)/music/track7.ogg
	-$(RM) -d $(DESTDIR)$(datadir)/music
	-$(RM) $(DESTDIR)$(datadir)/models/and_back_off.json
	-$(RM) $(DESTDIR)$(datadir)/models/and_back_on.json
	-$(RM) $(DESTDIR)$(datadir)/models/and_down_off.json
	-$(RM) $(DESTDIR)$(datadir)/models/and_down_on.json
	-$(RM) $(DESTDIR)$(datadir)/models/and_front_off.json
	-$(RM) $(DESTDIR)$(datadir)/models/and_front_on.json
	-$(RM) $(DESTDIR)$(datadir)/models/and_left_off.json
	-$(RM) $(DESTDIR)$(datadir)/models/and_left_on.json
	-$(RM) $(DESTDIR)$(datadir)/models/and_right_off.json
	-$(RM) $(DESTDIR)$(datadir)/models/and_right_on.json
	-$(RM) $(DESTDIR)$(datadir)/models/and_up_off.json
	-$(RM) $(DESTDIR)$(datadir)/models/and_up_on.json
	-$(RM) $(DESTDIR)$(datadir)/models/buffer_back_off.json
	-$(RM) $(DESTDIR)$(datadir)/models/buffer_back_on.json
	-$(RM) $(DESTDIR)$(datadir)/models/buffer_down_off.json
	-$(RM) $(DESTDIR)$(datadir)/models/buffer_down_on.json
	-$(RM) $(DESTDIR)$(datadir)/models/buffer_front_off.json
	-$(RM) $(DESTDIR)$(datadir)/models/buffer_front_on.json
	-$(RM) $(DESTDIR)$(datadir)/models/buffer_left_off.json
	-$(RM) $(DESTDIR)$(datadir)/models/buffer_left_on.json
	-$(RM) $(DESTDIR)$(datadir)/models/buffer_right_off.json
	-$(RM) $(DESTDIR)$(datadir)/models/buffer_right_on.json
	-$(RM) $(DESTDIR)$(datadir)/models/buffer_up_off.json
	-$(RM) $(DESTDIR)$(datadir)/models/buffer_up_on.json
	-$(RM) $(DESTDIR)$(datadir)/models/button_off.json
	-$(RM) $(DESTDIR)$(datadir)/models/button_on.json
	-$(RM) $(DESTDIR)$(datadir)/models/checkpoint.json
	-$(RM) $(DESTDIR)$(datadir)/models/crate.json
	-$(RM) $(DESTDIR)$(datadir)/models/cursor.json
	-$(RM) $(DESTDIR)$(datadir)/models/engine.json
	-$(RM) $(DESTDIR)$(datadir)/models/fixed.json
	-$(RM) $(DESTDIR)$(datadir)/models/fixed2.json
	-$(RM) $(DESTDIR)$(datadir)/models/fixed3.json
	-$(RM) $(DESTDIR)$(datadir)/models/fixed4.json
	-$(RM) $(DESTDIR)$(datadir)/models/fixed5.json
	-$(RM) $(DESTDIR)$(datadir)/models/goal.json
	-$(RM) $(DESTDIR)$(datadir)/models/hover.json
	-$(RM) $(DESTDIR)$(datadir)/models/index.json
	-$(RM) $(DESTDIR)$(datadir)/models/lift.json
	-$(RM) $(DESTDIR)$(datadir)/models/nand_back_off.json
	-$(RM) $(DESTDIR)$(datadir)/models/nand_back_on.json
	-$(RM) $(DESTDIR)$(datadir)/models/nand_down_off.json
	-$(RM) $(DESTDIR)$(datadir)/models/nand_down_on.json
	-$(RM) $(DESTDIR)$(datadir)/models/nand_front_off.json
	-$(RM) $(DESTDIR)$(datadir)/models/nand_front_on.json
	-$(RM) $(DESTDIR)$(datadir)/models/nand_left_off.json
	-$(RM) $(DESTDIR)$(datadir)/models/nand_left_on.json
	-$(RM) $(DESTDIR)$(datadir)/models/nand_right_off.json
	-$(RM) $(DESTDIR)$(datadir)/models/nand_right_on.json
	-$(RM) $(DESTDIR)$(datadir)/models/nand_up_off.json
	-$(RM) $(DESTDIR)$(datadir)/models/nand_up_on.json
	-$(RM) $(DESTDIR)$(datadir)/models/nor_back_off.json
	-$(RM) $(DESTDIR)$(datadir)/models/nor_back_on.json
	-$(RM) $(DESTDIR)$(datadir)/models/nor_down_off.json
	-$(RM) $(DESTDIR)$(datadir)/models/nor_down_on.json
	-$(RM) $(DESTDIR)$(datadir)/models/nor_front_off.json
	-$(RM) $(DESTDIR)$(datadir)/models/nor_front_on.json
	-$(RM) $(DESTDIR)$(datadir)/models/nor_left_off.json
	-$(RM) $(DESTDIR)$(datadir)/models/nor_left_on.json
	-$(RM) $(DESTDIR)$(datadir)/models/nor_right_off.json
	-$(RM) $(DESTDIR)$(datadir)/models/nor_right_on.json
	-$(RM) $(DESTDIR)$(datadir)/models/nor_up_off.json
	-$(RM) $(DESTDIR)$(datadir)/models/nor_up_on.json
	-$(RM) $(DESTDIR)$(datadir)/models/not_back_off.json
	-$(RM) $(DESTDIR)$(datadir)/models/not_back_on.json
	-$(RM) $(DESTDIR)$(datadir)/models/not_down_off.json
	-$(RM) $(DESTDIR)$(datadir)/models/not_down_on.json
	-$(RM) $(DESTDIR)$(datadir)/models/not_front_off.json
	-$(RM) $(DESTDIR)$(datadir)/models/not_front_on.json
	-$(RM) $(DESTDIR)$(datadir)/models/not_left_off.json
	-$(RM) $(DESTDIR)$(datadir)/models/not_left_on.json
	-$(RM) $(DESTDIR)$(datadir)/models/not_right_off.json
	-$(RM) $(DESTDIR)$(datadir)/models/not_right_on.json
	-$(RM) $(DESTDIR)$(datadir)/models/not_up_off.json
	-$(RM) $(DESTDIR)$(datadir)/models/not_up_on.json
	-$(RM) $(DESTDIR)$(datadir)/models/or_back_off.json
	-$(RM) $(DESTDIR)$(datadir)/models/or_back_on.json
	-$(RM) $(DESTDIR)$(datadir)/models/or_down_off.json
	-$(RM) $(DESTDIR)$(datadir)/models/or_down_on.json
	-$(RM) $(DESTDIR)$(datadir)/models/or_front_off.json
	-$(RM) $(DESTDIR)$(datadir)/models/or_front_on.json
	-$(RM) $(DESTDIR)$(datadir)/models/or_left_off.json
	-$(RM) $(DESTDIR)$(datadir)/models/or_left_on.json
	-$(RM) $(DESTDIR)$(datadir)/models/or_right_off.json
	-$(RM) $(DESTDIR)$(datadir)/models/or_right_on.json
	-$(RM) $(DESTDIR)$(datadir)/models/or_up_off.json
	-$(RM) $(DESTDIR)$(datadir)/models/or_up_on.json
	-$(RM) $(DESTDIR)$(datadir)/models/player_fb.json
	-$(RM) $(DESTDIR)$(datadir)/models/player_lr.json
	-$(RM) $(DESTDIR)$(datadir)/models/plug_off.json
	-$(RM) $(DESTDIR)$(datadir)/models/plug_on.json
	-$(RM) $(DESTDIR)$(datadir)/models/wire_off.json
	-$(RM) $(DESTDIR)$(datadir)/models/wire_on.json
	-$(RM) $(DESTDIR)$(datadir)/models/xnor_back_off.json
	-$(RM) $(DESTDIR)$(datadir)/models/xnor_back_on.json
	-$(RM) $(DESTDIR)$(datadir)/models/xnor_down_off.json
	-$(RM) $(DESTDIR)$(datadir)/models/xnor_down_on.json
	-$(RM) $(DESTDIR)$(datadir)/models/xnor_front_off.json
	-$(RM) $(DESTDIR)$(datadir)/models/xnor_front_on.json
	-$(RM) $(DESTDIR)$(datadir)/models/xnor_left_off.json
	-$(RM) $(DESTDIR)$(datadir)/models/xnor_left_on.json
	-$(RM) $(DESTDIR)$(datadir)/models/xnor_right_off.json
	-$(RM) $(DESTDIR)$(datadir)/models/xnor_right_on.json
	-$(RM) $(DESTDIR)$(datadir)/models/xnor_up_off.json
	-$(RM) $(DESTDIR)$(datadir)/models/xnor_up_on.json
	-$(RM) $(DESTDIR)$(datadir)/models/xor_back_off.json
	-$(RM) $(DESTDIR)$(datadir)/models/xor_back_on.json
	-$(RM) $(DESTDIR)$(datadir)/models/xor_down_off.json
	-$(RM) $(DESTDIR)$(datadir)/models/xor_down_on.json
	-$(RM) $(DESTDIR)$(datadir)/models/xor_front_off.json
	-$(RM) $(DESTDIR)$(datadir)/models/xor_front_on.json
	-$(RM) $(DESTDIR)$(datadir)/models/xor_left_off.json
	-$(RM) $(DESTDIR)$(datadir)/models/xor_left_on.json
	-$(RM) $(DESTDIR)$(datadir)/models/xor_right_off.json
	-$(RM) $(DESTDIR)$(datadir)/models/xor_right_on.json
	-$(RM) $(DESTDIR)$(datadir)/models/xor_up_off.json
	-$(RM) $(DESTDIR)$(datadir)/models/xor_up_on.json
	-$(RM) -d $(DESTDIR)$(datadir)/models
	-$(RM) -d $(DESTDIR)$(datadir)

.PHONY: submodules
submodules:
	make -C $(root_dir)geom
	make -f $(root_dir)mdea/Makefile CC="$(CC)" AR="$(AR)"

edit: edit.o render.o geometry.o media.o context.o config.o model.o event.o libmdea.a
	$(CXX) $(CXXFLAGS) $(LDFLAGS) -o $@ $^ $(LOADLIBES) $(LDLIBS)

play: play.o render.o geometry.o media.o context.o config.o model.o event.o libmdea.a
	$(CXX) $(CXXFLAGS) $(LDFLAGS) -o $@ $^ $(LOADLIBES) $(LDLIBS)

png: $(root_dir)data/textures/textures.png

$(root_dir)data/textures/%.png: $(root_dir)workspace/%.svg
	inkscape -e $@ $<

ogg: $(root_dir)data/music/track0.ogg \
	$(root_dir)data/music/track1.ogg \
	$(root_dir)data/music/track2.ogg \
	$(root_dir)data/music/track3.ogg \
	$(root_dir)data/music/track4.ogg \
	$(root_dir)data/music/track5.ogg \
	$(root_dir)data/music/track6.ogg \
	$(root_dir)data/music/track7.ogg

$(root_dir)data/music/%.ogg: $(root_dir)workspace/%.mscz
	musescore $< --export-to $@

-include $(shell find -name "*.d")
