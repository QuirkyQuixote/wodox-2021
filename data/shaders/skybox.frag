#version 330 core

in vec3 frag_texcoords;

out vec4 color;

uniform samplerCube skybox;

void main()
{
        color = texture(skybox, frag_texcoords);
}
