#version 330 core

in vec3 frag_position;
in vec3 frag_normal;
in vec2 frag_texcoord;
in mat3 frag_tbn;

layout (location = 0) out vec4 color;
layout (location = 1) out vec4 bright;

uniform sampler2D texture1;
uniform float dark_level;
uniform vec3 camera;

void main()
{
        float alpha = texture(texture1, frag_texcoord).a;
        if (alpha == 0) discard;

        vec3 ambient = texture(texture1, frag_texcoord).xyz;
        vec3 diffuse = texture(texture1, frag_texcoord + vec2(0, 0.2)).xyz;
        vec3 specular = texture(texture1, frag_texcoord + vec2(0, 0.4)).xyz;
        float shininess = texture(texture1, frag_texcoord + vec2(0, 0.4)).a * 128;
        vec3 emissive = texture(texture1, frag_texcoord + vec2(0, 0.6)).xyz;

        vec3 normal = texture(texture1, frag_texcoord + vec2(0, 0.8)).xyz;
        normal = normal * 2.0 - 1;
        normal = normalize(frag_tbn * normal);

        vec3 light_dir = normalize(vec3(-1, 3, 2));
        vec3 view_dir = normalize(camera - frag_position);
        vec3 reflect_dir = reflect(-light_dir, normal);

        float y = (frag_position.y - dark_level) / 10.0;
        float attenuation = y / (y + 1);

        vec3 combined = emissive * 8;
        combined += ambient * attenuation;
        combined += diffuse * max(dot(normal, light_dir), 0) * attenuation;
        combined += specular * pow(max(dot(view_dir, reflect_dir), 0), shininess) * attenuation;

        bright.r = combined.r > 1 ? combined.r : 0;
        bright.g = combined.g > 1 ? combined.g : 0;
        bright.b = combined.b > 1 ? combined.b : 0;
        bright.a = alpha;
        color = vec4(combined, alpha);
}
