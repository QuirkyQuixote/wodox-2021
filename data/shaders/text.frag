#version 330 core

in vec2 frag_texcoords;

out vec4 color;

uniform sampler2D image;

void main()
{
        color = texture(image, frag_texcoords);
        if (color.a < 0.5) discard;
}
