#version 330 core

layout(location = 0) in vec3 vert_position;
layout(location = 1) in vec3 vert_normal;
layout(location = 2) in vec2 vert_texcoord;
layout(location = 3) in vec3 vert_tangent;
layout(location = 4) in vec3 vert_bitangent;

out vec3 frag_position;
out vec3 frag_normal;
out vec2 frag_texcoord;
out mat3 frag_tbn;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

void main()
{
        vec4 model_position = model * vec4(vert_position, 1);
	gl_Position = projection * view * model_position;
        frag_position = model_position.xyz;
        frag_normal = vert_normal;
        frag_texcoord = vert_texcoord;

        vec3 t = normalize(vec3(model * vec4(vert_tangent, 0.0)));
        vec3 b = normalize(vec3(model * vec4(vert_bitangent, 0.0)));
        vec3 n = normalize(vec3(model * vec4(vert_normal, 0.0)));
        frag_tbn = mat3(t, b, n);
}

