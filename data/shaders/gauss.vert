#version 330 core

layout (location = 0) in vec3 vert_position;
layout (location = 1) in vec2 vert_texcoords;

out vec2 frag_texcoords;

void main()
{
    frag_texcoords = vert_texcoords;
    gl_Position = vec4(vert_position, 1.0);
}


