#version 330 core

layout(location = 0) in vec2 vert_position;
layout(location = 1) in vec2 vert_texcoords;

out vec2 frag_texcoords;

uniform mat4 mvp;

void main()
{
        gl_Position = mvp * vec4(vert_position, 0.0f, 1.0f);
        frag_texcoords = vert_texcoords;
}
