#version 330 core

in vec2 frag_texcoords;

out vec4 color;

uniform sampler2D color_tex;

void main()
{
    vec3 col = texture(color_tex, frag_texcoords).rgb;
    color = vec4(col, 1.0);
}
