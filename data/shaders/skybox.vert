#version 330 core

layout (location = 0) in vec3 vert_position;

out vec3 frag_texcoords;

uniform mat4 MVP;

void main()
{
        frag_texcoords = vert_position;
        gl_Position = MVP * vec4(vert_position, 1.0);
}
