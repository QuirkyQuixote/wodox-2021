Wodox 2021
==========

This thing is a refactor of the Wodox game I coded a long time ago, not because
I want the game redone, but:

- because I want to see how I would change things if I tried to do it again,
- because I want an excuse to use the 3D capabilities of my Geom library,
- because I want to do some of tinkering with OpenGL and shaders,

Installation
------------

The code is written in 2020 ISO C++.  It uses the libraries `stdc++`, `GL`,
`GLM`, `GLEW`, `SDL2`, `SDL2_mixer`, and `SDL2_image`.
It has been tested natively in Debian 12, and successfully cross-compiled for
Windows.

```shell
sudo apt install build-essential
sudo apt install libsdl2-dev libsdl2-image-dev libsdl2-mixer-dev
sudo apt install libgl1-mesa-dev libglm-dev libglew-dev
```

By default, the make scripts installs everything in `prefix=$(HOME)/.local`,
with the executable in `bindir=$(prefix)/bin` and the data in
`datadir=$(prefix)/share/wodox_2021`; this can be changed by providing
alternative values for `prefix`, `bindir`, or `datadir` to make, but note below
about the paths where the executable searches for data.

```shell
make all install
```

The executable searches for data in a `wodox2021` directory on paths according
to the XDG base directory specification, in order:

- wherever `$XDG_DATA_HOME` says, or if not defined `$HOME/.local/share`;
- wherever `$XDG_DATA_DIRS` says, or if not defined `/usr/local/share:/usr/share`.

Usage
-----

The actual game and the editor are separate files:

- launch the game with `wodox2021-play <level file>`,
- launch the editor with `wodox2021-edit <level file>`.

Both binaries allow interactive configuration of the controls with the
`-i, --input` option.

Default controls are, for the game:

|action                 |alternatives   |       |       |               |
|---                    |---            |---    |---    |---            |
|move left              |left cursor    |a      |j      |gamepad 0 left |
|move right             |right cursor   |d      |l      |gamepad 0 right|
|move up                |up cursor      |w      |i      |gamepad 0 up   |
|move down              |down cursor    |s      |k      |gamepad 0 down |
|rotate camera cw       |z              |q      |u      |gamepad 0 y    |
|rotate camera ccw      |x              |e      |o      |gamepad 0 b    |
|exit                   |escape         |       |       |               |

And for the editor:

|action         |alternatives           |
|---            |---                    |
|move left      |a                      |
|move right     |d                      |
|move front     |w                      |
|move back      |s                      |
|move up        |spacebar               |
|move down      |left control           |
|paint          |right mouse button     |
|erase          |left mouse button      |
|pick color     |middle mouse button    |
|exit           |escape                 |


